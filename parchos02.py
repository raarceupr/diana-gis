'''
Determines the cities whose geometries "touch". 
'''

import sys, random, time
from optparse import OptionParser

#
# Add the paths where the qgis libraries live... So that we 
# certain to find all the functions.
# I copied this from the output of running
# /Applications/QGIS.app/Contents/MacOS/QGIS -nologo --code test.py
#
sys.path = ['/Applications/QGIS.app/Contents/MacOS/../Resources/python/plugins/processing', 
'/Applications/QGIS.app/Contents/MacOS/../Resources/python', 
u'/Users/rarce/.qgis2/python', 
u'/Users/rarce/.qgis2/python/plugins', 
'/Applications/QGIS.app/Contents/MacOS/../Resources/python/plugins', 
'/Library/Frameworks/SQLite3.framework/Versions/C/Python/2.7', 
'/Library/Frameworks/GEOS.framework/Versions/3/Python/2.7', 
'/Library/Python/2.7/site-packages/matplotlib-override', 
'/Library/Frameworks/GDAL.framework/Versions/1.11/Python/2.7/site-packages', 
'/Library/Python/2.7/site-packages/virtualenv-1.8.2-py2.7.egg',
'/Library/Python/2.7/site-packages/virtualenvwrapper-3.6-py2.7.egg', 
'/Library/Python/2.7/site-packages/stevedore-0.4-py2.7.egg', 
'/Library/Python/2.7/site-packages/virtualenv_clone-0.2.4-py2.7.egg', 
'/Library/Python/2.7/site-packages/distribute-0.6.28-py2.7.egg', 
'/Library/Python/2.7/site-packages/pip-1.2.1-py2.7.egg', 
'/Library/Python/2.7/site-packages/Flask-0.9-py2.7.egg', 
'/Library/Python/2.7/site-packages/Jinja2-2.6-py2.7.egg', 
'/Library/Python/2.7/site-packages/Werkzeug-0.8.3-py2.7.egg', 
'/Library/Python/2.7/site-packages/pydot-1.0.28-py2.7.egg', 
'/Library/Python/2.7/site-packages/python_graph_core-1.8.2-py2.7.egg', 
'/Library/Python/2.7/site-packages/python_graph_dot-1.8.2-py2.7.egg', 
'/Library/Frameworks/cairo.framework/Versions/1/Python/2.7', 
'/System/Library/Frameworks/Python.framework/Versions/2.7/lib/python27.zip', 
'/System/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7', 
'/System/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/plat-darwin', 
'/System/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/plat-mac', 
'/System/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/plat-mac/lib-scriptpackages', 
'/System/Library/Frameworks/Python.framework/Versions/2.7/Extras/lib/python', 
'/System/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/lib-tk', 
'/System/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/lib-old', 
'/System/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/lib-dynload', 
'/System/Library/Frameworks/Python.framework/Versions/2.7/Extras/lib/python/PyObjC', 
'/Library/Python/2.7/site-packages', 
'/Library/Python/2.7/site-packages/setuptools-0.6c11-py2.7.egg-info', 
'/Applications/QGIS.app/Contents/Resources/python/plugins/fTools/tools',
'/Users/rarce/Box Sync/uprpp/research/diana/2014F/source/PyQuadTree']


from osgeo import ogr
import os, sys
import pyqtree

inLayer = []

class Node:
    def __init__(self,id,b):
        self.id = id
        self.bbox = b

'''
Given the input layer, this computes the bounding rectangle. 
'''
def getGlobalBBox():
    fI = inLayer.GetFeature(0)
    fIGeom = fI.GetGeometryRef()
    fIBBox = fIGeom.GetEnvelope()
    xMin, yMin, xMax, yMax = fIBBox[0], fIBBox[2], fIBBox[1], fIBBox[3] 
    for i in range(0, inLayer.GetFeatureCount()):
        fI = inLayer.GetFeature(i)
        fIGeom = fI.GetGeometryRef()
        fIBBox = fIGeom.GetEnvelope()
        if fIBBox[0] < xMin: xMin = fIBBox[0] 
        if fIBBox[2] < yMin: yMin = fIBBox[2]
        if fIBBox[1] > xMax: xMax = fIBBox[1] 
        if fIBBox[3] < yMax: yMax = fIBBox[3]

    print "\tConsidered %d nodes" % inLayer.GetFeatureCount()
    return [xMin, yMin, xMax, yMax]

'''
Given the global bounding rectangle, inserts the items into a quadTree 
'''
def createAndInsertAll(globalBBox):
    items = []
    qTree = pyqtree.Index(bbox=globalBBox) 
    for i in range(0, 200): #inLayer.GetFeatureCount()):
        fI = inLayer.GetFeature(i)
        fIGeom = fI.GetGeometryRef()
        fIBBox = fIGeom.GetEnvelope()
        item = Node(fI.GetField("ID"), 
            [fIBBox[0], fIBBox[2], fIBBox[1], fIBBox[3]]) 
        items.append(item)

    random.shuffle(items)

    i = 0
    for item in items:
        qTree.insert(item=item, bbox=item.bbox)
        print "Inserting %d" % item.id
        print i, qTree.countmembers()
        i = i + 1

'''
Creates the output layer and data source. Also creates the fields.
'''
def createOutputLayer(myOutDriver, myOutShapeFile):
    myOutDataSource = myOutDriver.CreateDataSource(myOutShapeFile)
    myOutLayer      = myOutDataSource.CreateLayer(myOutShapeFile, 
                            geom_type=ogr.wkbLineString)

    outFields = [["SourceID", ogr.OFTInteger],
                 ["DestID",   ogr.OFTInteger],
                 ["SourceX",  ogr.OFTReal],
                 ["SourceY",  ogr.OFTReal],
                 ["DestX",    ogr.OFTReal],
                 ["DestY",    ogr.OFTReal]]

    for f in outFields:
        aNewField = ogr.FieldDefn(f[0], f[1] )
        myOutLayer.CreateField(aNewField)    

    return myOutDataSource, myOutLayer

'''
Given an input layer create an output layer of vertices for every two
nodes that "touch" (share boundaries)
'''

def joinByBoundary(myInLayer, myOutLayer, myDistance):
    myOutLayerDefn = myOutLayer.GetLayerDefn()
    
    if myDistance == None: myDistance = -1
    else: myDistance = int(myDistance)

    for i in range(0, myInLayer.GetFeatureCount()):
        # get the feature and its geometry
        fI = myInLayer.GetFeature(i)
        fIName = fI.GetField("ID")
        fIGeom = fI.GetGeometryRef()
        fIBBox = fIGeom.GetEnvelope()

        inFeature = myInLayer.GetFeature(i)
        outFeature = ogr.Feature(myOutLayerDefn)

        for j in range(i+1, inLayer.GetFeatureCount()): 
            # get the feature and its geometry
            fJ = myInLayer.GetFeature(j)
            fJName = fJ.GetField("ID")
            fJGeom = fJ.GetGeometryRef()
            
            # if the distance was None, then we join the nodes that are "touching

            # fIGeom.Centroid().Distance(fJGeom.Centroid())
            if ((myDistance >= 0 and fIGeom.Distance(fJGeom) < int(myDistance)) \
                or (myDistance == -1 and fI.GetGeometryRef().Touches(fJ.GetGeometryRef()) ) ):
        
                print fIName, "touches", fJName,
                print " Distance: ", fIGeom.Distance(fJGeom), 
                print " Centroid Distance: ", fIGeom.Centroid().Distance(fJGeom.Centroid())
                
                # create a line from centroid of city i to city j
                multiline = ogr.Geometry(ogr.wkbLineString)
                multiline.AddPoint(fIGeom.Centroid().GetX(),fIGeom.Centroid().GetY())
                multiline.AddPoint(fJGeom.Centroid().GetX(),fJGeom.Centroid().GetY())

                # add the multiline as a feature to the output layer
                outFeature = ogr.Feature(outLayerDefn)            
                outFeature.SetGeometry(multiline)

                # add the rest of the features to the line
                outFeature.SetField("SourceID" , fIName)
                outFeature.SetField("DestID"   , fJName)
                outFeature.SetField("SourceY"  , fIGeom.Centroid().GetY())
                outFeature.SetField("SourceX"  , fIGeom.Centroid().GetX())
                outFeature.SetField("SourceY"  , fIGeom.Centroid().GetY())
                outFeature.SetField("DestX"    , fJGeom.Centroid().GetX())
                outFeature.SetField("DestY"    , fJGeom.Centroid().GetY())

                outLayer.CreateFeature(outFeature)


def defineCLOptions():
    parser = OptionParser()
    parser.add_option("-i", "--inputFile")
    parser.add_option("-o", "--outputFile")
    parser.add_option("-d", "--distance")

    return parser.parse_args()

def validArguments(myOptions):
    if myOptions.inputFile == None:
        print "Please specify input file!"
        return False
    
    if not(os.path.isfile(myOptions.inputFile)):
        print "No such file exists (%s)" % myOptions.inputFile
        return False

    if myOptions.outputFile == None:
        print "Please specify output file!"
        return False
    
    return True

if __name__ == "__main__":
    # Gather the command line parameters
    # inShapefile = "../data/LIMITES_LEGALES_MUNICIPIOS_EDICION_MARZO2009/LIMITES_LEGALES_MUNICIPIOS_EDICION_MARZO2009.shp"
    # outShapefile = "/tmp/test.shp"


    (options, args) = defineCLOptions()

    if not(validArguments(options)): sys.exit(1)

    # Get the input Layer
    inDriver     = ogr.GetDriverByName("ESRI Shapefile")
    inDataSource = inDriver.Open(options.inputFile, 0)
    inLayer      = inDataSource.GetLayer()

    # Create the output Layer
    outDriver    = ogr.GetDriverByName("ESRI Shapefile")

    # Remove output shapefile if it already exists
    if os.path.exists(options.outputFile):
        outDriver.DeleteDataSource(options.outputFile)

    outDataSource, outLayer = createOutputLayer(outDriver,options.outputFile)

    # Get the output Layer's Feature Definition
    outLayerDefn = outLayer.GetLayerDefn()


    startTime = time.time()

    joinByBoundary(inLayer, outLayer, options.distance)
    #joinByDistance(inLayer, outLayer)

    print "..... done in ", time.time() - startTime


    # Close DataSources
    inDataSource.Destroy()
    outDataSource.Destroy()
