// grid.cpp
#include "grid.h"
using namespace std;

Grid::Grid(const Rectangle &originalBBox, int maxTicks) {
	_originalBBox = originalBBox;
	_maxTicks = maxTicks;

	int width = originalBBox.width();
	int height = originalBBox.height();

	if (width < height) {
		_scale = static_cast<double>(height)/_maxTicks;
		_yTicks = _maxTicks;
		_xTicks = static_cast<int>( ceil( width / _scale));		
	}
	else {
		_scale = static_cast<double>(width)/_maxTicks;
		_xTicks = _maxTicks;
		_yTicks = static_cast<int>( ceil( height / _scale));		
	}

	cout << "xTicks, yTicks, scale:" << _xTicks << ", " << _yTicks << ", " << _scale << endl; 

	_array.resize(_xTicks);
	for (int i = 0; i < _xTicks; i++)
		_array[i].resize(_yTicks);

};

void Grid::mapNode(int id, const Rectangle &bbox) {
	int xMin, xMax, yMin, yMax;

	cout << "BBOX xMin, yMin, xMax, yMax:" << bbox.topLeft().first << ", " << bbox.topLeft().second 
		 << ", " <<  bbox.botRight().first << ", " << bbox.botRight().second << endl;

	xMin = static_cast<int> ( ( bbox.topLeft().first - _originalBBox.topLeft().first ) / _scale );
	yMin = static_cast<int> ( ( bbox.topLeft().second - _originalBBox.topLeft().second ) / _scale );
	xMax = static_cast<int> ( ( bbox.botRight().first - _originalBBox.topLeft().first ) / _scale );
	yMax = static_cast<int> ( ( bbox.botRight().second - _originalBBox.topLeft().second ) / _scale );

	cout << "xMin, yMin, xMax, yMax:" << xMin << ", " << yMin << ", " << xMax << ", " << yMax << endl;

	if (xMin > 0) xMin = xMin - 1;
	if (yMin > 0) yMin = yMin - 1;

	if (xMax < _xTicks - 1) xMax = xMax + 1;
	if (yMax < _yTicks - 1) yMax = yMax + 1;

	for (int y = yMin; y <= yMax; y++) {
		for (int x = xMin; x <= xMax; x++) {
			_array[x][y].push_back(id);
		}
	}

	_cellList[id] = Rectangle(Point(xMin,yMin),Point(xMax,yMax)); //[xMin, yMin, xMax, yMax]
}
/*

	def mapNode(self,id, bbox):
		# print "original bbox:", self.originalBBox
		# print "this bbox:", bbox
		# print "scale:", self.scale
		xMin = int ( ( bbox[0] - self.originalBBox[0] ) / self.scale ) 
		yMin = int ( ( bbox[1] - self.originalBBox[1] ) / self.scale ) 
		xMax = int ( ( bbox[2] - self.originalBBox[0] ) / self.scale ) 
		yMax = int (( bbox[3] - self.originalBBox[1] ) / self.scale ) 

		if xMin > 0: xMin = xMin - 1
		if yMin > 0: yMin = yMin - 1
		if xMax < self.xTicks - 1: xMax = xMax + 1
		if yMax < self.yTicks - 1: yMax = yMax + 1

		# for r in self.array:
		# 	print r
		# print "mapNode indices: ", xMin, yMin, xMax, yMax
		for j in range(yMin, yMax+1):
			for i in range(xMin, xMax+1):
				# print "setting ", i, j
				cell = self.array[j][i]
				cell.append(id)				

		#print self.array
		self.cellList[id] = [xMin, yMin, xMax, yMax]
		*/
/*
class Grid:
	def __init__(self, originalBBox, maxTicks):
		self.originalBBox = originalBBox
		self.maxTicks = maxTicks

		#initialize the two dimensional array
		self.array = []
		self.cellList = {}
		
		width  = self.originalBBox[2] - self.originalBBox[0]
		height = self.originalBBox[3] - self.originalBBox[1]

		if (width < height):
			self.scale = float(height) / maxTicks
			self.yTicks = maxTicks
			self.xTicks = int( math.ceil(width / self.scale ))
		else:
			self.scale  = float(width) / maxTicks
			self.xTicks = maxTicks
			self.yTicks = int( math.ceil(height / self.scale ) ) 

		print self.xTicks, self.yTicks, self.scale

		for i in range(self.yTicks + 1):
			tmpL = []
			for j in range(self.xTicks + 1):
				tmpL.append([])
			self.array.append(tmpL)

		# for r in self.array:
		# 	print r

	# Given an id and its bounding box, adds the id to the cells and
	# stores the mapped bounding box.

	def mapNode(self,id, bbox):
		# print "original bbox:", self.originalBBox
		# print "this bbox:", bbox
		# print "scale:", self.scale
		xMin = int ( ( bbox[0] - self.originalBBox[0] ) / self.scale ) 
		yMin = int ( ( bbox[1] - self.originalBBox[1] ) / self.scale ) 
		xMax = int ( ( bbox[2] - self.originalBBox[0] ) / self.scale ) 
		yMax = int (( bbox[3] - self.originalBBox[1] ) / self.scale ) 

		if xMin > 0: xMin = xMin - 1
		if yMin > 0: yMin = yMin - 1
		if xMax < self.xTicks - 1: xMax = xMax + 1
		if yMax < self.yTicks - 1: yMax = yMax + 1

		# for r in self.array:
		# 	print r
		# print "mapNode indices: ", xMin, yMin, xMax, yMax
		for j in range(yMin, yMax+1):
			for i in range(xMin, xMax+1):
				# print "setting ", i, j
				cell = self.array[j][i]
				cell.append(id)				

		#print self.array
		self.cellList[id] = [xMin, yMin, xMax, yMax]

	# Given an id, returns the id's of other nodes that occupy
	# the same cells

	def hits(self,id):
		# print "hitting for %d" % id
		hitSet = set([])
		[xMin,yMin,xMax,yMax] = self.cellList[id]
		# print "bbox:", [xMin,yMin,xMax,yMax]
		for i in range(yMin, yMax + 1):
			for j in range(xMin, xMax + 1):
		#		print i, j, self.array[j][i]
				hitSet = hitSet.union(set(self.array[i][j]))  
				
		hitSet = hitSet.difference([id]) 
		#print hitSet
		return hitSet


if __name__ == "__main__":
	g = Grid([0,0,100,100],100)

	ctr = 0

	for i in range(100):
		L = []
		for j in range(2):
			L.append(random.random() * 50);
		L.append(L[0] + int(random.random() * 10))
		L.append(L[1] + int(random.random() * 10))

		#print L
		g.mapNode(ctr, L)
		ctr = ctr + 1


		# g.mapNode(777, [20,20,23,23])
		# g.mapNode(888, [90,10,93,13])
		# g.mapNode(999, [0,0,93,93])
	h = g.hits(3)
	print len(h), h

	# print g.cellList

	*/