'''
Given a file of georeferenced edges, creates another shapefile with just the nodes.
'''

import sys, random, time
from optparse import OptionParser
import grid
#import pydot

#
# Add the paths where the qgis libraries live... So that we 
# certain to find all the functions.
# I copied this from the output of running
# /Applications/QGIS.app/Contents/MacOS/QGIS -nologo --code test.py
#
sys.path = ['/Applications/QGIS.app/Contents/MacOS/../Resources/python/plugins/processing', 
'/Applications/QGIS.app/Contents/MacOS/../Resources/python', 
u'/Users/rarce/.qgis2/python', 
u'/Users/rarce/.qgis2/python/plugins', 
'/Applications/QGIS.app/Contents/MacOS/../Resources/python/plugins', 
'/Library/Frameworks/SQLite3.framework/Versions/C/Python/2.7', 
'/Library/Frameworks/GEOS.framework/Versions/3/Python/2.7', 
'/Library/Python/2.7/site-packages/matplotlib-override', 
'/Library/Frameworks/GDAL.framework/Versions/1.11/Python/2.7/site-packages', 
'/Library/Python/2.7/site-packages/virtualenv-1.8.2-py2.7.egg',
'/Library/Python/2.7/site-packages/virtualenvwrapper-3.6-py2.7.egg', 
'/Library/Python/2.7/site-packages/stevedore-0.4-py2.7.egg', 
'/Library/Python/2.7/site-packages/virtualenv_clone-0.2.4-py2.7.egg', 
'/Library/Python/2.7/site-packages/distribute-0.6.28-py2.7.egg', 
'/Library/Python/2.7/site-packages/pip-1.2.1-py2.7.egg', 
'/Library/Python/2.7/site-packages/Flask-0.9-py2.7.egg', 
'/Library/Python/2.7/site-packages/Jinja2-2.6-py2.7.egg', 
'/Library/Python/2.7/site-packages/Werkzeug-0.8.3-py2.7.egg', 
'/Library/Python/2.7/site-packages/pydot-1.0.28-py2.7.egg', 
'/Library/Python/2.7/site-packages/python_graph_core-1.8.2-py2.7.egg', 
'/Library/Python/2.7/site-packages/python_graph_dot-1.8.2-py2.7.egg', 
'/Library/Frameworks/cairo.framework/Versions/1/Python/2.7', 
'/System/Library/Frameworks/Python.framework/Versions/2.7/lib/python27.zip', 
'/System/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7', 
'/System/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/plat-darwin', 
'/System/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/plat-mac', 
'/System/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/plat-mac/lib-scriptpackages', 
'/System/Library/Frameworks/Python.framework/Versions/2.7/Extras/lib/python', 
'/System/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/lib-tk', 
'/System/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/lib-old', 
'/System/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/lib-dynload', 
'/System/Library/Frameworks/Python.framework/Versions/2.7/Extras/lib/python/PyObjC', 
'/Library/Python/2.7/site-packages', 
'/Library/Python/2.7/site-packages/setuptools-0.6c11-py2.7.egg-info', 
'/Applications/QGIS.app/Contents/Resources/python/plugins/fTools/tools',
'/Users/rarce/Box Sync/uprpp/research/diana/2014F/source/PyQuadTree']


from osgeo import ogr
import os, sys
import pyqtree

inLayer = []


'''
Given the global bounding rectangle, inserts the items into a quadTree 
'''
def createAndInsertAll(globalBBox):
    items = []
    qTree = pyqtree.Index(bbox=globalBBox) 
    for i in range(0, 200): #inLayer.GetFeatureCount()):
        fI = inLayer.GetFeature(i)
        fIGeom = fI.GetGeometryRef()
        fIBBox = fIGeom.GetEnvelope()
        item = Node(fI.GetField("ID"), 
            [fIBBox[0], fIBBox[2], fIBBox[1], fIBBox[3]]) 
        items.append(item)

    random.shuffle(items)

    i = 0
    for item in items:
        qTree.insert(item=item, bbox=item.bbox)
        print "Inserting %d" % item.id
        print i, qTree.countmembers()
        i = i + 1

'''
Creates the output layer and data source. Also creates the fields.
'''
def createOutputLayer(myOutDriver, myOutShapeFile):
    myOutDataSource = myOutDriver.CreateDataSource(myOutShapeFile)
    myOutLayer      = myOutDataSource.CreateLayer(myOutShapeFile, 
                            geom_type=ogr.wkbPoint)

    outFields = [["NodeID", ogr.OFTInteger] ] #,
                 #["GRIDCODE",  ogr.OFTInteger],
                 #["mimicProb",  ogr.OFTReal],
                 #["patches",  ogr.OFTInteger]]

    for f in outFields:
        aNewField = ogr.FieldDefn(f[0], f[1] )
        print "Creating output Column: %s" % f[0]
        myOutLayer.CreateField(aNewField)    

    return myOutDataSource, myOutLayer

'''
Just read through all the edges and add unique nodes, meanwhile
count the node degrees.
'''

M = {}

def readAll2Nodes(myInLayer, myOutLayer):
    myOutLayerDefn = myOutLayer.GetLayerDefn()
    

    # graph = pydot.Dot(graph_type='graph')

    for i in range(0, myInLayer.GetFeatureCount()):
        # get the feature and its geometry
        fI = myInLayer.GetFeature(i)
        #fIName = fI.GetField("ID")
        fIGeom = fI.GetGeometryRef()
        fIBBox = fIGeom.GetEnvelope()

        inFeature = myInLayer.GetFeature(i)
        outFeature = ogr.Feature(myOutLayerDefn)

        #print  fI.GetField("SourceID"), fI.GetField("DestID") 

        src = [fI.GetField("SourceX"),fI.GetField("SourceY")]
        dst = [fI.GetField("DestX"),fI.GetField("DestY")]


        srcID = fI.GetField("SourceID")
        dstID = fI.GetField("DestID")

        if srcID not in M:
            M[srcID] = 0

            point = ogr.Geometry(ogr.wkbPoint)
            point.AddPoint(src[0],src[1])

            outFeature = ogr.Feature(outLayerDefn)            
            outFeature.SetGeometry(point)
            
            outFeature.SetField("NodeID" , srcID)
            outLayer.CreateFeature(outFeature)


        M[srcID] = M[srcID] + 1

        if dstID not in M:
            M[dstID] = 0

            point = ogr.Geometry(ogr.wkbPoint)
            point.AddPoint(dst[0],dst[1])

            outFeature = ogr.Feature(outLayerDefn)            
            outFeature.SetGeometry(point)
            outFeature.SetField("NodeID" , dstID)
            outLayer.CreateFeature(outFeature)


        M[dstID] = M[dstID] + 1

        # print src,dst
        # print fIGeom.Centroid()

    print "Nodes created:" ,len(M)


def joinByBoundary(myInLayer, myOutLayer, myDistance, myGrid, myDotOutFile):
    myOutLayerDefn = myOutLayer.GetLayerDefn()
    
    if myDistance == None: myDistance = -1
    else: myDistance = int(myDistance)

    # graph = pydot.Dot(graph_type='graph')


    for i in range(0, myInLayer.GetFeatureCount()):
        # get the feature and its geometry
        fI = myInLayer.GetFeature(i)
        fIName = fI.GetField("ID")
        fIGeom = fI.GetGeometryRef()
        fIBBox = fIGeom.GetEnvelope()

        inFeature = myInLayer.GetFeature(i)
        outFeature = ogr.Feature(myOutLayerDefn)

        hits = myGrid.hits(i)
        print "possible hits:", len(hits)
        remHits = set([])
        for e in list(hits):
            if int(e) <= i: 
                remHits.add(e)
        hits = hits.difference(remHits)

        for j in hits: #range(i+1, inLayer.GetFeatureCount()): 
            # get the feature and its geometry
            fJ = myInLayer.GetFeature(j)
            fJName = fJ.GetField("ID")
            fJGeom = fJ.GetGeometryRef()


            srcNodeClass = nodeProperties[i]
            
            # if the distance was None, then we join the nodes that are "touching

            # fIGeom.Centroid().Distance(fJGeom.Centroid())
            if ((myDistance >= 0 and fIGeom.Distance(fJGeom) < int(myDistance)) \
                or (myDistance == -1 and fI.GetGeometryRef().Touches(fJ.GetGeometryRef()) ) ):
        
                print fIName, "touches", fJName,

                dstNodeClass = nodeProperties[j]

                if edgeExistsWOBiotic(srcNodeClass, dstNodeClass):

                    #print " Distance: ", fIGeom.Distance(fJGeom), 
                    #print " Centroid Distance: ", fIGeom.Centroid().Distance(fJGeom.Centroid())
                    
                    # create a line from centroid of city i to city j
                    multiline = ogr.Geometry(ogr.wkbLineString)
                    multiline.AddPoint(fIGeom.Centroid().GetX(),fIGeom.Centroid().GetY())
                    multiline.AddPoint(fJGeom.Centroid().GetX(),fJGeom.Centroid().GetY())

                    # add the multiline as a feature to the output layer
                    outFeature = ogr.Feature(outLayerDefn)            
                    outFeature.SetGeometry(multiline)



                    # add the rest of the features to the line
                    outFeature.SetField("SourceID" , fIName)
                    outFeature.SetField("DestID"   , fJName)
                    outFeature.SetField("SourceY"  , fIGeom.Centroid().GetY())
                    outFeature.SetField("SourceX"  , fIGeom.Centroid().GetX())
                    outFeature.SetField("SourceY"  , fIGeom.Centroid().GetY())
                    outFeature.SetField("DestX"    , fJGeom.Centroid().GetX())
                    outFeature.SetField("DestY"    , fJGeom.Centroid().GetY())
                    outFeature.SetField("SrcClass", nodeProperties[i])
                    outFeature.SetField("DstClass", nodeProperties[j])
                    #outFeature.SetField("GRIDCODE", nodeProperties[j])

                    outLayer.CreateFeature(outFeature)

                    edge = pydot.Edge(fIName, fJName)
                    graph.add_edge(edge)

    f = open(myDotOutFile, 'w')
    f.write(graph.to_string())
    f.close()


def defineCLOptions():
    parser = OptionParser()
    parser.add_option("-i", "--inputFile")
    parser.add_option("-o", "--outputFile")


    return parser.parse_args()

def validArguments(myOptions):
    if myOptions.inputFile == None:
        print "Please specify input file!"
        return False
    
    if not(os.path.isfile(myOptions.inputFile)):
        print "No such file exists (%s)" % myOptions.inputFile
        return False

    if myOptions.outputFile == None:
        print "Please specify output file!"
        return False


    return True

if __name__ == "__main__":

    (options, args) = defineCLOptions()

    if not(validArguments(options)): sys.exit(1)

    # Get the input Layer
    inDriver     = ogr.GetDriverByName("ESRI Shapefile")
    inDataSource = inDriver.Open(options.inputFile, 0)
    inLayer      = inDataSource.GetLayer()

    # Create the output Layer
    outDriver    = ogr.GetDriverByName("ESRI Shapefile")

    # Remove output shapefile if it already exists
    if os.path.exists(options.outputFile):
        outDriver.DeleteDataSource(options.outputFile)

    outDataSource, outLayer = createOutputLayer(outDriver,options.outputFile)

    # Get the output Layer's Feature Definition
    outLayerDefn = outLayer.GetLayerDefn()

    # globalBBox = getGlobalBBox()
    # g = grid.Grid(globalBBox,1000)

    # insertAllToGrid(g)

    # print nodeProperties

    # sys.exit(0)
    startTime = time.time()

    readAll2Nodes(inLayer, outLayer)
    #joinByDistance(inLayer, outLayer)

    print "..... done in ", time.time() - startTime


    # Close DataSources
    inDataSource.Destroy()
    outDataSource.Destroy()
