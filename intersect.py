'''
Determines the cities whose geometries "touch". 
'''

import sys

#
# Add the paths where the qgis libraries live... So that we 
# certain to find all the functions.
# I copied this from the output of running
# /Applications/QGIS.app/Contents/MacOS/QGIS -nologo --code test.py
#
sys.path = ['/Applications/QGIS.app/Contents/MacOS/../Resources/python/plugins/processing', 
'/Applications/QGIS.app/Contents/MacOS/../Resources/python', 
u'/Users/rarce/.qgis2/python', 
u'/Users/rarce/.qgis2/python/plugins', 
'/Applications/QGIS.app/Contents/MacOS/../Resources/python/plugins', 
'/Library/Frameworks/SQLite3.framework/Versions/C/Python/2.7', 
'/Library/Frameworks/GEOS.framework/Versions/3/Python/2.7', 
'/Library/Python/2.7/site-packages/matplotlib-override', 
'/Library/Frameworks/GDAL.framework/Versions/1.11/Python/2.7/site-packages', 
'/Library/Python/2.7/site-packages/virtualenv-1.8.2-py2.7.egg',
'/Library/Python/2.7/site-packages/virtualenvwrapper-3.6-py2.7.egg', 
'/Library/Python/2.7/site-packages/stevedore-0.4-py2.7.egg', 
'/Library/Python/2.7/site-packages/virtualenv_clone-0.2.4-py2.7.egg', 
'/Library/Python/2.7/site-packages/distribute-0.6.28-py2.7.egg', 
'/Library/Python/2.7/site-packages/pip-1.2.1-py2.7.egg', 
'/Library/Python/2.7/site-packages/Flask-0.9-py2.7.egg', 
'/Library/Python/2.7/site-packages/Jinja2-2.6-py2.7.egg', 
'/Library/Python/2.7/site-packages/Werkzeug-0.8.3-py2.7.egg', 
'/Library/Python/2.7/site-packages/pydot-1.0.28-py2.7.egg', 
'/Library/Python/2.7/site-packages/python_graph_core-1.8.2-py2.7.egg', 
'/Library/Python/2.7/site-packages/python_graph_dot-1.8.2-py2.7.egg', 
'/Library/Frameworks/cairo.framework/Versions/1/Python/2.7', 
'/System/Library/Frameworks/Python.framework/Versions/2.7/lib/python27.zip', 
'/System/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7', 
'/System/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/plat-darwin', 
'/System/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/plat-mac', 
'/System/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/plat-mac/lib-scriptpackages', 
'/System/Library/Frameworks/Python.framework/Versions/2.7/Extras/lib/python', 
'/System/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/lib-tk', 
'/System/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/lib-old', 
'/System/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/lib-dynload', 
'/System/Library/Frameworks/Python.framework/Versions/2.7/Extras/lib/python/PyObjC', 
'/Library/Python/2.7/site-packages', 
'/Library/Python/2.7/site-packages/setuptools-0.6c11-py2.7.egg-info', 
'/Applications/QGIS.app/Contents/Resources/python/plugins/fTools/tools']


from osgeo import ogr
import os

# Get the input Layer
inShapefile = "../data/LIMITES_LEGALES_MUNICIPIOS_EDICION_MARZO2009/LIMITES_LEGALES_MUNICIPIOS_EDICION_MARZO2009.shp"

inDriver     = ogr.GetDriverByName("ESRI Shapefile")
inDataSource = inDriver.Open(inShapefile, 0)
inLayer      = inDataSource.GetLayer()

# Create the output Layer
outShapefile = "caca.shp"
outDriver    = ogr.GetDriverByName("ESRI Shapefile")

# Remove output shapefile if it already exists
if os.path.exists(outShapefile):
    outDriver.DeleteDataSource(outShapefile)

# Create the output shapefile
outDataSource = outDriver.CreateDataSource(outShapefile)
outLayer      = outDataSource.CreateLayer(outShapefile, 
                        geom_type=ogr.wkbLineString)

# Add input Layer Fields to the output Layer
inLayerDefn   = inLayer.GetLayerDefn()
for i in range(0, inLayerDefn.GetFieldCount()):
    fieldDefn = inLayerDefn.GetFieldDefn(i)
    outLayer.CreateField(fieldDefn)

# Get the output Layer's Feature Definition
outLayerDefn = outLayer.GetLayerDefn()

#
# For every two cities, determine if their geometries touch
# 
for i in range(0, inLayer.GetFeatureCount()):
    # get the feature and its geometry
    fI = inLayer.GetFeature(i)
    fIName = fI.GetField("Municipio")
    fIGeom = fI.GetGeometryRef()

    for j in range(i+1, inLayer.GetFeatureCount()):
        # get the feature and its geometry
        fJ = inLayer.GetFeature(j)
        fJName = fJ.GetField("Municipio")
        fJGeom = fJ.GetGeometryRef()
        
        # determine if the touch, if so...
        if fI.GetGeometryRef().Touches(fJ.GetGeometryRef()):
    
            print fIName, "touches", fJName
            
            # create a line from centroid of city i to city j
            multiline = ogr.Geometry(ogr.wkbLineString)
            multiline.AddPoint(fIGeom.Centroid().GetX(),fIGeom.Centroid().GetY())
            multiline.AddPoint(fJGeom.Centroid().GetX(),fJGeom.Centroid().GetY())

            # add the multiline as a feature to the output layer
            outFeature = ogr.Feature(outLayerDefn)            
            outFeature.SetGeometry(multiline)
            outLayer.CreateFeature(outFeature)




# Close DataSources
inDataSource.Destroy()
outDataSource.Destroy()
