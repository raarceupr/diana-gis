/*
 * polysplit.cpp -- split polygons into constituents using OGR and GEOS
 *
 * written by Schuyler Erle <schuyler@nocat.net>
 * copyright (c) 2012 Geoloqi, Inc.
 * published under the 3-clause BSD license -- see README.txt for details.
 * 
 */



#include "polysplit.h"



void split_polygons(OGRPolyList *pieces, OGRGeometry* geometry, int max_vertices) {
    /* split_polygons recursively splits the (multi)polygon into smaller
     * polygons until each polygon has at most max_vertices, and pushes each
     * one onto the pieces vector.
     * 
     * Multipolygons are automatically divided into their constituent polygons.
     * Empty polygons and other geometry types are ignored. Invalid polygons
     * get cleaned up to the best of our ability, but this does trigger
     * warnings from inside GEOS.
     *
     * Each polygon is split by dividing its bounding box into quadrants, and
     * then recursing on the intersection of each quadrant with the original
     * polygon, until the pieces are of the desired complexity.
     */

    if (geometry == NULL) {
        std::cerr << "WARNING: NULL geometry passed to split_polygons!\n";
        return;
    }
    
    if (geometry->IsEmpty())
        return;
    
    if (geometry->getGeometryType() == wkbMultiPolygon) {
        OGRMultiPolygon *multi = (OGRMultiPolygon*) geometry;
        for (int i = 0; i < multi->getNumGeometries(); i++) {
            split_polygons(pieces, multi->getGeometryRef(i), max_vertices);
        }
        return;
    } 
    
    if (geometry->getGeometryType() != wkbPolygon)
        return;
    
    OGRPolygon* polygon = (OGRPolygon*) geometry;
    if (polygon->getExteriorRing()->getNumPoints() <= max_vertices) {
        pieces->push_back((OGRPolygon*) polygon->clone());
        return;
    }

    bool polygonIsPwned = false;
    if (!polygon->IsValid() || !polygon->IsSimple()) {
        polygon = (OGRPolygon*) polygon->Buffer(0); // try to tidy it up
        polygonIsPwned = true; // now we own the reference and have to free it later
    }

    OGRPoint centroid;
    polygon->Centroid(&centroid);
    double cornerX = centroid.getX(),
           cornerY = centroid.getY();

    OGREnvelope envelope;
    polygon->getEnvelope(&envelope);

    for (int quadrant = 0; quadrant < 4; quadrant++) {
        OGREnvelope bbox(envelope);
        OGRLinearRing ring;
        OGRPolygon mask;
        switch (quadrant) { // in no particular order, actually
            case 0: bbox.MaxX = cornerX; bbox.MaxY = cornerY; break;
            case 1: bbox.MaxX = cornerX; bbox.MinY = cornerY; break;
            case 2: bbox.MinX = cornerX; bbox.MaxY = cornerY; break;
            case 3: bbox.MinX = cornerX; bbox.MinY = cornerY; break;
        }
        ring.setNumPoints(5);
        ring.setPoint(0, bbox.MinX, bbox.MinY);
        ring.setPoint(1, bbox.MinX, bbox.MaxY);
        ring.setPoint(2, bbox.MaxX, bbox.MaxY);
        ring.setPoint(3, bbox.MaxX, bbox.MinY);
        ring.setPoint(4, bbox.MinX, bbox.MinY); // close the ring
        mask.addRing(&ring);
        OGRGeometry* piece = mask.Intersection(polygon);
        split_polygons(pieces, piece, max_vertices);
        delete piece;
    } 

    if (polygonIsPwned) delete polygon;
}

OGRDataSource *create_destination(const char* drivername, const char* filename,
        const char *layername, const char *id_field_name) {

    /* Find the requested OGR output driver. */
    OGRSFDriver* driver;
    driver = OGRSFDriverRegistrar::GetRegistrar()->GetDriverByName(drivername);
    if( driver == NULL ) {
        std::cerr << drivername << " driver not available.\n";
        return NULL;
    }

    /* Create the output data source. */
    OGRDataSource* ds = driver->CreateDataSource( filename, NULL );
    if( ds == NULL ) {
        std::cerr << "Creation of output file " << filename << " failed.\n";
        return NULL;
    }

    /* Create the output layer. */
    OGRLayer* layer;
    layer = ds->CreateLayer( layername, NULL, OUTPUTTYPE, NULL );
    if( layer == NULL ) {
        std::cerr << "Layer creation failed.\n";
        return NULL;
    }

    /* Add the ID field, which defaults to "id" if a name isn't given. */
    if (id_field_name == NULL) id_field_name = IDFIELD;
    OGRFieldDefn field( id_field_name, OFTInteger );
    if( layer->CreateField( &field ) != OGRERR_NONE ) {
        std::cerr <<  "Creating " << id_field_name << " field failed.\n";
        return NULL;
    }
    return ds;
}

void write_feature(OGRLayer *layer, OGRPolygon *geom, feature_id_t id) {
    /* Create a new feature from the geometry and ID, and write it to the
     * output layer. */
    OGRFeature *feature = OGRFeature::CreateFeature( layer->GetLayerDefn() );
    feature->SetField(0, id);
    feature->SetGeometryDirectly(geom); // saves having to destroy it manually
    if(layer->CreateFeature( feature ) != OGRERR_NONE) {
        std::cerr << "Failed to create feature in output.\n";
        exit( 1 );
    }
    OGRFeature::DestroyFeature( feature );
}

