'''
Determines the cities whose geometries "touch". 
'''

import sys, random, time
from optparse import OptionParser
import grid
# import pydot

#
# Add the paths where the qgis libraries live... So that we 
# certain to find all the functions.
# I copied this from the output of running
# /Applications/QGIS.app/Contents/MacOS/QGIS -nologo --code test.py
#
sys.path = ['/Applications/QGIS.app/Contents/MacOS/../Resources/python/plugins/processing', 
'/Applications/QGIS.app/Contents/MacOS/../Resources/python', 
u'/Users/rarce/.qgis2/python', 
u'/Users/rarce/.qgis2/python/plugins', 
'/Applications/QGIS.app/Contents/MacOS/../Resources/python/plugins', 
'/Library/Frameworks/SQLite3.framework/Versions/C/Python/2.7', 
'/Library/Frameworks/GEOS.framework/Versions/3/Python/2.7', 
'/Library/Python/2.7/site-packages/matplotlib-override', 
'/Library/Frameworks/GDAL.framework/Versions/1.11/Python/2.7/site-packages', 
'/Library/Python/2.7/site-packages/virtualenv-1.8.2-py2.7.egg',
'/Library/Python/2.7/site-packages/virtualenvwrapper-3.6-py2.7.egg', 
'/Library/Python/2.7/site-packages/stevedore-0.4-py2.7.egg', 
'/Library/Python/2.7/site-packages/virtualenv_clone-0.2.4-py2.7.egg', 
'/Library/Python/2.7/site-packages/distribute-0.6.28-py2.7.egg', 
'/Library/Python/2.7/site-packages/pip-1.2.1-py2.7.egg', 
'/Library/Python/2.7/site-packages/Flask-0.9-py2.7.egg', 
'/Library/Python/2.7/site-packages/Jinja2-2.6-py2.7.egg', 
'/Library/Python/2.7/site-packages/Werkzeug-0.8.3-py2.7.egg', 
'/Library/Python/2.7/site-packages/pydot-1.0.28-py2.7.egg', 
'/Library/Python/2.7/site-packages/python_graph_core-1.8.2-py2.7.egg', 
'/Library/Python/2.7/site-packages/python_graph_dot-1.8.2-py2.7.egg', 
'/Library/Frameworks/cairo.framework/Versions/1/Python/2.7', 
'/System/Library/Frameworks/Python.framework/Versions/2.7/lib/python27.zip', 
'/System/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7', 
'/System/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/plat-darwin', 
'/System/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/plat-mac', 
'/System/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/plat-mac/lib-scriptpackages', 
'/System/Library/Frameworks/Python.framework/Versions/2.7/Extras/lib/python', 
'/System/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/lib-tk', 
'/System/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/lib-old', 
'/System/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/lib-dynload', 
'/System/Library/Frameworks/Python.framework/Versions/2.7/Extras/lib/python/PyObjC', 
'/Library/Python/2.7/site-packages', 
'/Library/Python/2.7/site-packages/setuptools-0.6c11-py2.7.egg-info', 
'/Applications/QGIS.app/Contents/Resources/python/plugins/fTools/tools',
'/Users/rarce/Box Sync/uprpp/research/diana/2014F/source/PyQuadTree']


from osgeo import ogr
import os, sys

# wkt1 = "POLYGON ((0.0 0.0, 10.0 0.0, 10.0 10.0, 0.0 10.0, 0.0 0.0))"

wkt1 = "POLYGON ((0.0 0.0, 10.0 0.0, 10.0 10.0, 0.0 0.0))"

wkt2 = "POLYGON ((0.0 5.0, 10.0 5.0, 10.0 15.0, 0.0 15.0, 0.0 5.0))"

# wkt1 = "POLYGON ((1200000.0 600000.0, 1205000.0 600000.0, 1205000.0 600600.0, 1200000.0 600600.0, 1200000.0 600000.0))"
# wkt2 = "POLYGON ((1100000.0 600000.0, 1205000.0 600000.0, 1205000.0 600600.0, 1100000.0 600600.0, 1100000.0 600000.0))"
#wkt1 = "POLYGON ((1199915.6662253144 633079.3410163528, 1199915.6662253144 614453.958118695, 1219317.1067437078 614453.958118695, 1219317.1067437078 633079.3410163528, 1199915.6662253144 633079.3410163528)))"

#wkt2 = "POLYGON ((1199915.6662253144 633079.3410163528, 1199915.6662253144 614453.958118695, 1219317.1067437078 614453.958118695, 1219317.1067437078 633079.3410163528, 1199915.6662253144 633079.3410163528)))"

poly1 = ogr.CreateGeometryFromWkt(wkt1)
poly2 = ogr.CreateGeometryFromWkt(wkt2)

print "The bounding box is:", poly1.GetEnvelope()


union = poly1.Union(poly2)

#print poly1
#print poly2
print union.ExportToWkt()

intersection = poly1.Intersection(poly2)
print intersection.ExportToWkt()

diff = poly1.Difference(poly2)
print diff.ExportToWkt()

diff2 = poly2.Difference(poly1)
print diff2.ExportToWkt()

