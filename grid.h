
#include <cmath>
#include <vector>
#include <map>
#include <iostream>

using namespace std;

typedef std::pair <int,int> Point;


class Rectangle {
public:
	Rectangle() {
	}
	Rectangle(const Point &topLeft, const Point &botRight) {
		_topLeft = topLeft;
		_botRight = botRight;
	}


	void setRandom(int maxX, int maxY) {
		cout << "maxX, maxY: " <<  maxX << "," <<  maxY << endl;
		_topLeft.first = rand() % maxX; 
		_topLeft.second = rand() % maxY; 
		_botRight.first = _topLeft.first + ( rand() % (maxX - _topLeft.first) ); 
		_botRight.second = _topLeft.second + ( rand() %  (maxY - _topLeft.second) ); 
		cout << _topLeft.first << ","  << _topLeft.second << "," <<  _botRight.first  << "," << _botRight.second;

	}
	int width() const  { return _botRight.first - _topLeft.first; }
	int height() const { return _botRight.second - _topLeft.second; }
	Point topLeft() const  {return _topLeft;}
	Point botRight() const {return _botRight;}

private:
	Point _topLeft, _botRight;
};

class Grid {
private:
	Rectangle _originalBBox;
	int _maxTicks;
	vector < vector < vector < int > > > _array;
	map < int, Rectangle > _cellList;
	double _scale;
	int _xTicks, _yTicks;
public:
	Grid(const Rectangle &originalBBox, int maxTicks);
	void mapNode(int id, const Rectangle &bbox);

	//mapNode(self,id, bbox):
};
