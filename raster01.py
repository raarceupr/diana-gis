import sys



import struct
import numpy as np
from optparse import OptionParser
import os
from bisect import *

from rasterstats import zonal_stats

from sysPaths import *

from osgeo import gdal, ogr

print sys.path 

import osr





L = [1, 12, 30, 40, 50]


'''
Given a pixels current value returns a classification between 0 and len(L)-1.
'''
def classifyPixels(n):
    i = find_index_le(L,n)
    if i == -1: return 10
    return i

'''
Given list a and element x returns rightmost value less than or equal to x
'''
def find_le(a, x):
    i = bisect_right(a, x)
    if i:
        return a[i-1]
    return -1

'''
Given list a and element x returns the index of the rightmost value less
than or equal to x
'''
def find_index_le(a, x):
    e = find_le(a, x)
    if e == -1: return -1
    return a.index(e)


'''
Define the command line options
'''
def defineCLOptions():
    parser = OptionParser()
    parser.add_option("-i", "--inputFile")
    parser.add_option("-o", "--outputFile")
    parser.add_option("-r", "--classRanges")
    parser.add_option("-s", "--shapeFile")
    # parser.add_option("-d", "--distance")
    # parser.add_option("-g", "--outputDotFile")

    return parser.parse_args()

'''
Given a structure with the options that user provided, returns true if
they are valid.
'''
def validArguments(myOptions):
    if myOptions.inputFile == None:
        print "Please specify input file!"
        return False
    
    if not(os.path.isfile(myOptions.inputFile)):
        print "No such file exists (%s)" % myOptions.inputFile
        return False

    if myOptions.classRanges == None:
        print "Please specify ranges!"
        return False

    if myOptions.outputFile == None:
        print "Please specify output file!"
        return False

    if myOptions.shapeFile == None:
        print "Please specify shape file!"
        return False

    return True


if __name__ == "__main__":

    (options, args) = defineCLOptions()

    if not(validArguments(options)): sys.exit(1)

    # convert string to list of integers
    L = map(lambda x: int(x), options.classRanges[1:-1].split(','))

    # open the input file
    src_ds = gdal.Open( options.inputFile )

    # obtain several important features from the input raster
    cols = src_ds.RasterXSize
    rows = src_ds.RasterYSize
    bands = src_ds.RasterCount
    datatype = src_ds.GetRasterBand(1).DataType

    # lets get the driver needed to create the output raster
    driver = gdal.GetDriverByName('HFA')
    driver.Register()

    dest_ds = driver.Create(options.outputFile, cols, rows, bands, gdal.GDT_UInt16)

    # georeference the output raster identical to input
    geoTransform = src_ds.GetGeoTransform()
    dest_ds.SetGeoTransform(geoTransform )
    proj = dest_ds.GetProjection()
    dest_ds.SetProjection(proj)

    outBand = dest_ds.GetRasterBand(1)
    srcband = src_ds.GetRasterBand(1)
    
    print "srcband.XSize:", srcband.XSize, 
    print "srcband.YSize:", srcband.YSize

    scanline = srcband.ReadRaster( 0, 0, srcband.XSize, srcband.YSize, \
                                         srcband.XSize, srcband.YSize, \
                                         gdal.GDT_UInt16  )

    # print "len scan", len(scanline)

    tuple_of_int16 = struct.unpack('H' * srcband.XSize * srcband.YSize, scanline)
    
    # read the tuple into a numpy array
    array = np.array(tuple_of_int16)

    # classify !!!
    array = map(lambda x: classifyPixels(x), array)

    # reshape into a 2-dim array
    array = np.reshape(array, (srcband.YSize, srcband.XSize))

    for r in array:
        print str(r)

    outBand.WriteArray(array, 0, 0) 
    outRasterSRS = osr.SpatialReference()
    outRasterSRS.ImportFromWkt(src_ds.GetProjectionRef())
    dest_ds.SetProjection(outRasterSRS.ExportToWkt())
    



    # polygonize 
    dst_layername = options.shapeFile
    drv = ogr.GetDriverByName("ESRI Shapefile")
    dst_ds = drv.CreateDataSource( dst_layername + ".shp" )
    dst_layer = dst_ds.CreateLayer(dst_layername, srs = None )

    gdal.Polygonize( outBand, None, dst_layer, -1, [], callback=None )

    dst_ds.Destroy()
    outBand.FlushCache()
    print "rasterstats ..."
    
    stats = zonal_stats(dst_layername + ".shp" , options.outputFile )
    print stats

    sys.exit(0)