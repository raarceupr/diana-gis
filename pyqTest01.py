import sys
import random
sys.path = ['/Users/rarce/Box Sync/uprpp/research/diana/2014F/source/PyQuadTree']

import pyqtree


class Node:
	def __init__(self,id,b):
		self.bbox = b
		self.id = id
	# def __getattr__(self, name):
	# 	return self.name

# class dotdict(dict):
# 	def __getattr__(self, name):
# 		return self[name]

items = []

print "Creating...."
for i in range(100000):
	xBL = random.random() * 100
	yBL = random.random() * 100

	xUR = xBL + random.random() * 2
	yUR = yBL + random.random() * 2
	if xUR > 100: xUR = 100
	if yUR > 100: yUR = 100

	items.append(Node(i,(xBL,yBL,xUR,yUR)))

#print items[0].bbox

	#insert(Node([random.randint()]))
#items = [Node([2,2,3,3]), Node([52,52,63,63])]

# print items
# sys.exit(0)
# a = Node([2,2,3,3])
# print a.bbox

print "Inserting.."
spindex = pyqtree.Index(bbox=[0,0,100,100]) 
i = 0
for item in items:
    spindex.insert(item=item, bbox=item.bbox)
    #print spindex.countmembers()
    i = i + 1
print "\t inserted %d items" % i

print "Counting.."
print spindex.countmembers()

print "Intersecting.."
overlapbbox = [1,1,2,2]
matches = spindex.intersect(overlapbbox)
print len(matches)