//gridClient.cpp

#include <iostream>
#include "grid.h"

using namespace std;

int main() {
	Point p0, p1;
	p0.first = p0.second = 0;
	p1.first = p1.second = 100;
	Grid g = Grid(Rectangle(p0,p1),100);

	int ctr = 0;

	vector< Rectangle > RA;

	RA.resize(100);
	for (int i = 0; i < RA.size(); i++) {
		RA[i].setRandom(p1.first, p1.second);
		g.mapNode(i, RA[i]);
	}

	// 	#print L
	// 	g.mapNode(ctr, L)
	// 	ctr = ctr + 1


	// 	# g.mapNode(777, [20,20,23,23])
	// 	# g.mapNode(888, [90,10,93,13])
	// 	# g.mapNode(999, [0,0,93,93])
	// h = g.hits(3)
	// print len(h), h
	return 0;
}