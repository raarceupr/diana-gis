'''
Given a shape file and a raster that contains probabilities for 
a give species, generate a raster that includes the average of 
probabilities for each polygon.
'''

'''
Step 2.  Attach vine patch information to habitat shapefile

1.  Use the Union tool to merge together the lc_index_Xc_poly with the vine patch polygon layer (VinePatchesAll).  
    Vine_network/raw_input_layers/VinePatchesAll.shp
    Allow gaps in the resulting shapefile generated from this step. 

2.  Delete the features with a hostID = 0. These are vine patches that fall outside the extent of the habitat map. They should be 4 features to delete. 

3.  Select features with hostID = 8759 AND PatchID < 1. Then merge together the 4 selected features****
****This only happens when running the two classes version.

Resulting shapefile layers names (in folder: Results_step2):
Two classes = host2c_w_patches
Three classes = host3c_w_patches

'''


import sys
import struct
import numpy as np
from optparse import OptionParser
import os
from bisect import *

from rasterstats import zonal_stats

from sysPaths import *
from osgeo import gdal, ogr


'''
Define the command line options
'''
def defineCLOptions():
    parser = OptionParser()
    parser.add_option("-r", "--inputProbRasterFile")
    parser.add_option("-c", "--inputClassRasterFile")
    parser.add_option("-s", "--inputShapeFile")         # this is the habitats shapefile
    parser.add_option("-o", "--outputShapeFile")
    parser.add_option("-p", "--species")
    parser.add_option("-a", "--inputPatchesShapeFile")


    return parser.parse_args()

'''
Given a structure with the options that user provided, returns true if
they are valid.
'''
def validArguments(myOptions):
    if myOptions.inputProbRasterFile == None:
        print "Please specify input probability raster file!"
        return False

    if myOptions.inputClassRasterFile == None:
        print "Please specify input class raster file!"
        return False
    
    if not(os.path.isfile(myOptions.inputProbRasterFile)):
        print "No such raster ile exists (%s)" % myOptions.inputProbRasterFile
        return False

    if not(os.path.isfile(myOptions.inputClassRasterFile)):
        print "No such raster ile exists (%s)" % myOptions.inputClassRasterFile
        return False

    if myOptions.inputPatchesShapeFile == None:
        print "Please specify input patches shape file!"
        return False

    if myOptions.inputShapeFile == None:
        print "Please specify input shape file!"
        return False
    
    if not(os.path.isfile(myOptions.inputShapeFile)):
        print "No such file exists (%s)" % myOptions.inputShapeFile
        return False

    if myOptions.outputShapeFile == None:
        print "Please specify output file!"
        return False


    if myOptions.species == None:
        print "Please specify species!"
        return False

    return True


'''
Creates the output layer and data source. Also creates the fields.
'''
def createOutputLayer(myOutDriver, myOutShapeFile, species):
    myOutDataSource = myOutDriver.CreateDataSource(myOutShapeFile)
    myOutLayer      = myOutDataSource.CreateLayer(myOutShapeFile, 
                            geom_type=ogr.wkbLineString)

    outFields = [["FID", ogr.OFTInteger], ["prob" + species, ogr.OFTReal], ["class" , ogr.OFTInteger]]

    for f in outFields:
        aNewField = ogr.FieldDefn(f[0], f[1] )
        myOutLayer.CreateField(aNewField)    

    return myOutDataSource, myOutLayer



if __name__ == "__main__":

    (options, args) = defineCLOptions()

    if not(validArguments(options)): sys.exit(1)

    # create the statistics
    stats = zonal_stats(options.inputShapeFile, options.inputProbRasterFile)

    classStats = zonal_stats(options.inputShapeFile, options.inputClassRasterFile)

    # print stats


    # Now do the rest of the stuff: create the output layer 
    # and update it with the statitics 

    # Get the input Layer
    inDriver     = ogr.GetDriverByName("ESRI Shapefile")
    inDataSource = inDriver.Open(options.inputShapeFile, 0)
    inLayer      = inDataSource.GetLayer()

    # Create the output Layer
    outDriver    = ogr.GetDriverByName("ESRI Shapefile")

    # Remove output shapefile if it already exists
    if os.path.exists(options.outputShapeFile):
        print "Removing the old: %s" % options.outputShapeFile
        outDriver.DeleteDataSource(options.outputShapeFile)

    outDataSource, outLayer = \
        createOutputLayer(outDriver,options.outputShapeFile, options.species)

    # Add input Layer Fields to the output Layer
    inLayerDefn   = inLayer.GetLayerDefn()

    # Get the output Layer's Feature Definition
    outLayerDefn = outLayer.GetLayerDefn()

    print "This features:", inLayer.GetFeatureCount()

    probFieldName = "prob" + options.species
    classFieldName = "class"

    # Add features to the ouput Layer

    print "Adding the class and probability features to the shapefile ..."
    for i in range(0, inLayer.GetFeatureCount()):
        inFeature = inLayer.GetFeature(i)
        outFeature = ogr.Feature(outLayerDefn)
        outFeature.SetField("FID", inFeature.GetField(0))
        outFeature.SetField(probFieldName, stats[i]['mean'])
        outFeature.SetField(classFieldName, int(classStats[i]['mean']))

        # print "Setting %s to  %d" % (outLayerDefn.GetFieldDefn(1).GetNameRef(), inFeature.GetField(0))

        geom = inFeature.GetGeometryRef()
        outFeature.SetGeometry(geom)
        outLayer.CreateFeature(outFeature)
    print "\tDone."

    print "Making the union....."


    sys.exit(0)
