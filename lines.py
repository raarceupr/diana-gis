import sys
sys.path = ['/Applications/QGIS.app/Contents/MacOS/../Resources/python/plugins/processing', '/Applications/QGIS.app/Contents/MacOS/../Resources/python', u'/Users/rarce/.qgis2/python', u'/Users/rarce/.qgis2/python/plugins', '/Applications/QGIS.app/Contents/MacOS/../Resources/python/plugins', '/Library/Frameworks/SQLite3.framework/Versions/C/Python/2.7', '/Library/Frameworks/GEOS.framework/Versions/3/Python/2.7', '/Library/Python/2.7/site-packages/matplotlib-override', '/Library/Frameworks/GDAL.framework/Versions/1.11/Python/2.7/site-packages', '/Library/Python/2.7/site-packages/virtualenv-1.8.2-py2.7.egg', '/Library/Python/2.7/site-packages/virtualenvwrapper-3.6-py2.7.egg', '/Library/Python/2.7/site-packages/stevedore-0.4-py2.7.egg', '/Library/Python/2.7/site-packages/virtualenv_clone-0.2.4-py2.7.egg', '/Library/Python/2.7/site-packages/distribute-0.6.28-py2.7.egg', '/Library/Python/2.7/site-packages/pip-1.2.1-py2.7.egg', '/Library/Python/2.7/site-packages/Flask-0.9-py2.7.egg', '/Library/Python/2.7/site-packages/Jinja2-2.6-py2.7.egg', '/Library/Python/2.7/site-packages/Werkzeug-0.8.3-py2.7.egg', '/Library/Python/2.7/site-packages/pydot-1.0.28-py2.7.egg', '/Library/Python/2.7/site-packages/python_graph_core-1.8.2-py2.7.egg', '/Library/Python/2.7/site-packages/python_graph_dot-1.8.2-py2.7.egg', '/Library/Frameworks/cairo.framework/Versions/1/Python/2.7', '/System/Library/Frameworks/Python.framework/Versions/2.7/lib/python27.zip', '/System/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7', '/System/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/plat-darwin', '/System/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/plat-mac', '/System/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/plat-mac/lib-scriptpackages', '/System/Library/Frameworks/Python.framework/Versions/2.7/Extras/lib/python', '/System/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/lib-tk', '/System/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/lib-old', '/System/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/lib-dynload', '/System/Library/Frameworks/Python.framework/Versions/2.7/Extras/lib/python/PyObjC', '/Library/Python/2.7/site-packages', '/Library/Python/2.7/site-packages/setuptools-0.6c11-py2.7.egg-info', '/Applications/QGIS.app/Contents/Resources/python/plugins/fTools/tools']


from osgeo import ogr
import os

# Get the input Layer
inShapefile = "/Users/rarce/Downloads/qgis_sample_data/shapefiles/lakes.shp"
inDriver = ogr.GetDriverByName("ESRI Shapefile")
inDataSource = inDriver.Open(inShapefile, 0)
inLayer = inDataSource.GetLayer()

# Create the output Layer
outShapefile = "lakes_lines.shp"
outDriver = ogr.GetDriverByName("ESRI Shapefile")

# Remove output shapefile if it already exists
if os.path.exists(outShapefile):
    outDriver.DeleteDataSource(outShapefile)

# Create the output shapefile
outDataSource = outDriver.CreateDataSource(outShapefile)
outLayer = outDataSource.CreateLayer("lakes_lines.shp", geom_type=ogr.wkbLineString)

# Add input Layer Fields to the output Layer
inLayerDefn = inLayer.GetLayerDefn()
for i in range(0, inLayerDefn.GetFieldCount()):
    fieldDefn = inLayerDefn.GetFieldDefn(i)
    outLayer.CreateField(fieldDefn)

# Get the output Layer's Feature Definition
outLayerDefn = outLayer.GetLayerDefn()

prevCentroid = None
# Add features to the ouput Layer
for i in range(0, inLayer.GetFeatureCount()):
    # Get the input Feature
    inFeature = inLayer.GetFeature(i)
    # Create output Feature
    outFeature = ogr.Feature(outLayerDefn)
    # Add field values from input Layer
    for i in range(0, outLayerDefn.GetFieldCount()):
        outFeature.SetField(outLayerDefn.GetFieldDefn(i).GetNameRef(), inFeature.GetField(i))
    # Set geometry as centroid
    geom = inFeature.GetGeometryRef()
    centroid = geom.Centroid()

    if prevCentroid != None:

        multiline = ogr.Geometry(ogr.wkbLineString)
        
        # tremendo marron para obtener las coordenadas
        pp = prevCentroid.ExportToWkt().replace("(","").replace(")","").split(" ")
        p = centroid.ExportToWkt().replace("(","").replace(")","").split(" ")

        print pp
        multiline.AddPoint(prevCentroid.GetX(),float(pp[2]))
        multiline.AddPoint(float(p[1]),float(p[2]))
        outFeature.SetGeometry(multiline)
        # Add new feature to output Layer
        outLayer.CreateFeature(outFeature)

    prevCentroid = centroid
# Close DataSources
inDataSource.Destroy()
outDataSource.Destroy()
