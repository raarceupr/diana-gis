CFLAGS := -O3 -Wall $(shell gdal-config --cflags) -std=c++11
# CFLAGS += -ggdb
LIBS := $(shell gdal-config --libs)


parchosUnion01: parchosUnion01.o polysplit.o
	g++ -o parchosUnion01 parchosUnion01.o    polysplit.o $(LIBS)

polysplit.o: polysplit.cpp polysplit.h
	g++ -c $(CFLAGS) polysplit.cpp $(LIBS)

parchosUnion01.o: parchosUnion01.cpp polysplit.h
	g++ -c $(CFLAGS) parchosUnion01.cpp $(LIBS)

parchosARed01: parchosARed01.o polysplit.o edge.o
	g++ -o parchosARed01 parchosARed01.o  edge.o polysplit.o $(LIBS)

polysplit.o: polysplit.cpp polysplit.h 
	g++ -c $(CFLAGS) polysplit.cpp $(LIBS)

edge.o: edge.cpp edge.h 
	g++ -c $(CFLAGS) edge.cpp $(LIBS)

parchosARed01.o: parchosARed01.cpp polysplit.h coefficients.h
	g++ -c $(CFLAGS) parchosARed01.cpp $(LIBS)



clean:
	rm -rf polysplit polysplit.dSYM/
