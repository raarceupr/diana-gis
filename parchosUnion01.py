#!/usr/bin/python
'''
In this version you may choose the type of table that is used for node/node relations
'''

import sys, random, time
from optparse import OptionParser
import grid


'''
This is done because at nanobio the modules are invoked in a slightly
different manner.
'''
import os
thisSystem = os.popen("hostname")
atNano =  (thisSystem.read().split(".")[0]=="nanobio")
# import pydot

#
# Add the paths where the qgis libraries live... So that we 
# certain to find all the functions.
# I copied this from the output of running
# /Applications/QGIS.app/Contents/MacOS/QGIS -nologo --code test.py
#
if atNano:
  sys.path.append('./PyQuadTree')
  import ogr
else:
    sys.path = ['/Applications/QGIS.app/Contents/MacOS/../Resources/python/plugins/processing', 
    '/Applications/QGIS.app/Contents/MacOS/../Resources/python', 
    u'/Users/rarce/.qgis2/python', 
    u'/Users/rarce/.qgis2/python/plugins', 
    '/Applications/QGIS.app/Contents/MacOS/../Resources/python/plugins', 
    '/Library/Frameworks/SQLite3.framework/Versions/C/Python/2.7', 
    '/Library/Frameworks/GEOS.framework/Versions/3/Python/2.7', 
    '/Library/Python/2.7/site-packages/matplotlib-override', 
    '/Library/Frameworks/GDAL.framework/Versions/1.11/Python/2.7/site-packages', 
    '/Library/Python/2.7/site-packages/virtualenv-1.8.2-py2.7.egg',
    '/Library/Python/2.7/site-packages/virtualenvwrapper-3.6-py2.7.egg', 
    '/Library/Python/2.7/site-packages/stevedore-0.4-py2.7.egg', 
    '/Library/Python/2.7/site-packages/virtualenv_clone-0.2.4-py2.7.egg', 
    '/Library/Python/2.7/site-packages/distribute-0.6.28-py2.7.egg', 
    '/Library/Python/2.7/site-packages/pip-1.2.1-py2.7.egg', 
    '/Library/Python/2.7/site-packages/Flask-0.9-py2.7.egg', 
    '/Library/Python/2.7/site-packages/Jinja2-2.6-py2.7.egg', 
    '/Library/Python/2.7/site-packages/Werkzeug-0.8.3-py2.7.egg', 
    '/Library/Python/2.7/site-packages/pydot-1.0.28-py2.7.egg', 
    '/Library/Python/2.7/site-packages/python_graph_core-1.8.2-py2.7.egg', 
    '/Library/Python/2.7/site-packages/python_graph_dot-1.8.2-py2.7.egg', 
    '/Library/Frameworks/cairo.framework/Versions/1/Python/2.7', 
    '/System/Library/Frameworks/Python.framework/Versions/2.7/lib/python27.zip', 
    '/System/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7', 
    '/System/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/plat-darwin', 
    '/System/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/plat-mac', 
    '/System/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/plat-mac/lib-scriptpackages', 
    '/System/Library/Frameworks/Python.framework/Versions/2.7/Extras/lib/python', 
    '/System/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/lib-tk', 
    '/System/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/lib-old', 
    '/System/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/lib-dynload', 
    '/System/Library/Frameworks/Python.framework/Versions/2.7/Extras/lib/python/PyObjC', 
    '/Library/Python/2.7/site-packages', 
    '/Library/Python/2.7/site-packages/setuptools-0.6c11-py2.7.egg-info', 
    '/Applications/QGIS.app/Contents/Resources/python/plugins/fTools/tools',
    '/Users/rarce/Box Sync/uprpp/research/diana/2014F/source/PyQuadTree']

    from osgeo import ogr

import sys
# import pyqtree

inLayer = []

class Node:
    def __init__(self,id,b):
        self.id = id
        self.bbox = b

'''
Given the input layer, this computes the bounding rectangle. 
'''
def getGlobalBBox():
    fI = inLayer.GetFeature(0)
    fIGeom = fI.GetGeometryRef()
    fIBBox = fIGeom.GetEnvelope()
    xMin, yMin, xMax, yMax = fIBBox[0], fIBBox[2], fIBBox[1], fIBBox[3] 
    for i in range(0, inLayer.GetFeatureCount()):
        fI = inLayer.GetFeature(i)
        fIGeom = fI.GetGeometryRef()
        fIBBox = fIGeom.GetEnvelope()
        if fIBBox[0] < xMin: xMin = fIBBox[0] 
        if fIBBox[2] < yMin: yMin = fIBBox[2]
        if fIBBox[1] > xMax: xMax = fIBBox[1] 
        if fIBBox[3] > yMax: yMax = fIBBox[3]

    print "\tConsidered %d nodes" % inLayer.GetFeatureCount()
    print "\tBbox: ",[xMin, yMin, xMax, yMax]     
    return [xMin, yMin, xMax, yMax]


# this is the array with the properties
nodeProperties = []

def insertAllToGrid(myGrid, options):
    for i in range(0, inLayer.GetFeatureCount()):
        fI = inLayer.GetFeature(i)
        fIGeom = fI.GetGeometryRef()
        fIBBox = fIGeom.GetEnvelope()
        print "Inserting %d" % fI.GetField("ID")
        myGrid.mapNode(i,[fIBBox[0],fIBBox[2],fIBBox[1],fIBBox[3]])
        # propTuple = [fI.GetField("ID"), fI.GetField("GRIDCODE"), fI.GetField("mimicProb"), fI.GetField("patches")]
        
        if (options.classes == "2"):
            propTuple = classifyNode_TwoClasses(fI.GetField("GRIDCODE"), fI.GetField("mimicProb"), fI.GetField("patches")) 
        elif (options.classes == "3"):
            propTuple = classifyNode_ThreeClasses(fI.GetField("GRIDCODE"), fI.GetField("mimicProb"), fI.GetField("patches")) 
        
        nodeProperties.append(propTuple)


'''
Given the susceptibility class, the probability and presence of patches
assigns a class for the node
'''
def classifyNode_TwoClasses(susClass, speciesProb, patchesPresent):
    if susClass == 2:
        if speciesProb >= 0.5:
            if patchesPresent:
                nodeClass = 1
            else: 
                nodeClass = 2
        else:
            if patchesPresent:
                nodeClass = 3
            else:
                nodeClass = 4
    else:
        if speciesProb >= 0.5:
            if patchesPresent:
                nodeClass = 5
            else:
                nodeClass = 6
        else:
            if patchesPresent:
                nodeClass = 7
            else:
                nodeClass = 8

    return nodeClass


def classifyNode_ThreeClasses(susClass, speciesProb, patchesPresent):
    if susClass == 3:
        if speciesProb >= 0.5:
            if patchesPresent:
                nodeClass = 1
            else:
                nodeClass = 2
        else:
            if patchesPresent:
                nodeClass = 3
            else:
                nodeClass = 4

    elif susClass == 2:
        if speciesProb >= 0.5:
            if patchesPresent:
                nodeClass = 5
            else:
                nodeClass = 6
        else:
            if patchesPresent:
                nodeClass = 7
            else:
                nodeClass = 8

    else:
        if speciesProb >= 0.5:
            if patchesPresent:
                nodeClass = 9
            else:
                nodeClass = 10
        else:
            if patchesPresent:
                nodeClass = 11
            else:
                nodeClass = 12

    return nodeClass


tblMMActual_2class   = [ [1.0,0.0,0.7,0.0,0.8,0.0,0.5,0.0] ,
                  [ -1,0.0,0.0,0.0,0.0,0.0,0.0,0.0] , 
                  [ -1, -1,0.4,0.0,0.5,0.0,0.2,0.0] ,
                  [ -1, -1, -1,0.0,0.0,0.0,0.0,0.0] , 
                  [ -1, -1, -1, -1,0.6,0.0,0.3,0.0] , 
                  [ -1, -1, -1, -1, -1,0.0,0.0,0.0] ,
                  [ -1, -1, -1, -1, -1, -1,0.0,0.0] ,
                  [ -1, -1, -1, -1, -1, -1, -1,0.0] ]

tblMMPotential_2class =[ [1.0,1.0,0.7,0.7,0.8,0.8,0.5,0.5] ,
                  [ -1,1.0,0.7,0.7,0.8,0.8,0.5,0.5] , 
                  [ -1, -1,0.4,0.4,0.5,0.5,0.2,0.2] ,
                  [ -1, -1, -1,0.4,0.5,0.5,0.2,0.2] , 
                  [ -1, -1, -1, -1,0.6,0.6,0.3,0.3] , 
                  [ -1, -1, -1, -1, -1,0.6,0.3,0.3] ,
                  [ -1, -1, -1, -1, -1, -1,0.0,0.0] ,
                  [ -1, -1, -1, -1, -1, -1, -1,0.0] ]

tblMMActual_3class = [[1.0,0.0,0.7,0.0,0.9,0.0,0.6,0.0,0.8,0.0,0.0,0.0] ,
                      [ -1,0.0,0.0,0.0,0.9,0.0,0.0,0.0,0.8,0.0,0.0,0.0] , 
                      [ -1, -1,0.4,0.0,0.6,0.0,0.3,0.0,0.5,0.0,0.2,0.0] ,
                      [ -1, -1, -1,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0] , 
                      [ -1, -1, -1, -1,0.8,0.0,0.5,0.0,0.7,0.0,0.4,0.0] , 
                      [ -1, -1, -1, -1, -1,0.0,0.0,0.0,0.0,0.0,0.0,0.0] ,
                      [ -1, -1, -1, -1, -1, -1,0.2,0.0,0.4,0.0,0.1,0.0] ,
                      [ -1, -1, -1, -1, -1, -1, -1,0.0,0.0,0.0,0.0,0.0] ,
                      [ -1, -1, -1, -1, -1, -1, -1, -1,0.6,0.0,0.3,0.0] ,
                      [ -1, -1, -1, -1, -1, -1, -1, -1, -1,0.0,0.0,0.0] ,
                      [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,0.0,0.0] ,
                      [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,0.0] ]


tblMMPotential_3class = [ [1.0,1.0,0.7,0.7,0.9,0.9,0.6,0.6,0.8,0.8,0.5,0.5] ,
                          [ -1,1.0,0.7,0.7,0.9,0.9,0.6,0.6,0.8,0.8,0.5,0.5] , 
                          [ -1, -1,0.4,0.4,0.6,0.6,0.3,0.3,0.5,0.5,0.2,0.2] ,
                          [ -1, -1, -1,0.4,0.6,0.6,0.3,0.3,0.5,0.5,0.2,0.2] , 
                          [ -1, -1, -1, -1,0.8,0.8,0.5,0.5,0.7,0.7,0.4,0.4] , 
                          [ -1, -1, -1, -1, -1,0.8,0.5,0.5,0.7,0.7,0.4,0.4] ,
                          [ -1, -1, -1, -1, -1, -1,0.2,0.2,0.4,0.4,0.1,0.1] ,
                          [ -1, -1, -1, -1, -1, -1, -1,0.2,0.4,0.4,0.1,0.1] ,
                          [ -1, -1, -1, -1, -1, -1, -1, -1,0.6,0.6,0.3,0.3] ,
                          [ -1, -1, -1, -1, -1, -1, -1, -1, -1,0.6,0.3,0.3] ,
                          [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,0.0,0.0] ,
                          [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,0.0] ]


tblWPosBiotic = [ [1.0,0.0,0.7,0.0,0.8,0.0,0.5,0.0] ,
                  [ -1,1.0,0.0,0.0,0.0,0.0,0.0,0.0] , 
                  [ -1, -1,1.0,0.0,0.5,0.0,0.2,0.0] ,
                  [ -1, -1, -1,1.0,0.0,0.0,0.0,0.0] , 
                  [ -1, -1, -1, -1,1.0,0.0,0.3,0.0] , 
                  [ -1, -1, -1, -1, -1,1.0,0.0,0.0] ,
                  [ -1, -1, -1, -1, -1, -1,1.0,0.0] ,
                  [ -1, -1, -1, -1, -1, -1, -1,1.0] ]


tblWOBiotic = [ [1.0,1.0,0.7,0.7,0.8,0.8,0.5,0.5] ,
                [ -1,1.0,0.7,0.7,0.8,0.8,0.5,0.5] , 
                [ -1, -1,0.4,0.4,0.5,0.5,0.2,0.2] ,
                [ -1, -1, -1,0.4,0.5,0.5,0.2,0.2] , 
                [ -1, -1, -1, -1,0.6,0.6,0.3,0.3] , 
                [ -1, -1, -1, -1, -1,0.6,0.3,0.3] ,
                [ -1, -1, -1, -1, -1, -1,0.0,0.0] ,
                [ -1, -1, -1, -1, -1, -1, -1,0.0] ]

# g1 is biotic
tblWNegBiotic = [ [1.0,0.0,0.0,0.7,0.8,0.8,0.0,0.5] ,
                  [ -1,1.0,0.0,0.7,0.8,0.8,0.0,0.5] , 
                  [ -1, -1,0.0,0.0,0.0,0.0,0.0,0.0] ,
                  [ -1, -1, -1,0.4,0.5,0.5,0.0,0.2] , 
                  [ -1, -1, -1, -1,0.6,0.6,0.0,0.3] , 
                  [ -1, -1, -1, -1, -1,0.6,0.0,0.3] ,
                  [ -1, -1, -1, -1, -1, -1,0.0,0.0] ,
                  [ -1, -1, -1, -1, -1, -1, -1,0.0] ]



# tblWPosBiotic = [ [1.0,0.0,0.7,0.0,0.9,0.0,0.6,0.0,0.8,0.0,0.5,0.0] ,
#                   [ -1,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0] , 
#                   [ -1, -1,0.4,0.0,0.6,0.0,0.3,0.0,0.5,0.0,0.2,0.0] ,
#                   [ -1, -1, -1,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0] , 
#                   [ -1, -1, -1, -1,0.8,0.0,0.5,0.0,0.7,0.0,0.4,0.0] , 
#                   [ -1, -1, -1, -1, -1,0.0,0.0,0.0,0.0,0.0,0.0,0.0] ,
#                   [ -1, -1, -1, -1, -1, -1,0.2,0.0,0.4,0.0,0.1,0.0] ,
#                   [ -1, -1, -1, -1, -1, -1, -1,0.0,0.0,0.0,0.0,0.0] ,
#                   [ -1, -1, -1, -1, -1, -1, -1, -1,0.6,0.0,0.3,0.0] ,
#                   [ -1, -1, -1, -1, -1, -1, -1, -1, -1,0.0,0.0,0.0] ,
#                   [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,0.0,0.0] ,
#                   [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,0.0] ]

# tblWNegBiotic = [ [1.0,1.0,0.0,0.7,0.9,0.9,0.0,0.6,0.8,0.8,0.0,0.5] ,
#                   [ -1,1.0,0.0,0.7,0.9,0.9,0.0,0.6,0.8,0.8,0.0,0.5] , 
#                   [ -1, -1,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0] ,
#                   [ -1, -1, -1,0.4,0.6,0.6,0.0,0.3,0.5,0.5,0.0,0.2] , 
#                   [ -1, -1, -1, -1,0.8,0.8,0.0,0.5,0.7,0.7,0.0,0.4] , 
#                   [ -1, -1, -1, -1, -1,0.8,0.0,0.5,0.7,0.7,0.0,0.4] ,
#                   [ -1, -1, -1, -1, -1, -1,0.0,0.0,0.0,0.0,0.0,0.0] ,
#                   [ -1, -1, -1, -1, -1, -1, -1,0.2,0.4,0.4,0.0,0.1] ,
#                   [ -1, -1, -1, -1, -1, -1, -1, -1,0.6,0.6,0.0,0.3] ,
#                   [ -1, -1, -1, -1, -1, -1, -1, -1, -1,0.6,0.0,0.3] ,
#                   [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,0.0,0.0] ,
#                   [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,0.0] ]

# tblW0Biotic =   [ [1.0,1.0,0.7,0.7,0.9,0.9,0.6,0.6,0.8,0.8,0.5,0.5] ,
#                   [ -1,1.0,0.7,0.7,0.9,0.9,0.6,0.6,0.8,0.8,0.5,0.5] , 
#                   [ -1, -1,0.4,0.4,0.6,0.6,0.3,0.3,0.5,0.5,0.2,0.2] ,
#                   [ -1, -1, -1,0.4,0.6,0.6,0.3,0.3,0.5,0.5,0.2,0.2] , 
#                   [ -1, -1, -1, -1,0.8,0.8,0.5,0.5,0.7,0.7,0.4,0.4] , 
#                   [ -1, -1, -1, -1, -1,0.8,0.5,0.5,0.7,0.7,0.4,0.4] ,
#                   [ -1, -1, -1, -1, -1, -1,0.2,0.2,0.4,0.4,0.1,0.1] ,
#                   [ -1, -1, -1, -1, -1, -1, -1,0.2,0.4,0.4,0.1,0.1] ,
#                   [ -1, -1, -1, -1, -1, -1, -1, -1,0.6,0.6,0.3,0.3] ,
#                   [ -1, -1, -1, -1, -1, -1, -1, -1, -1,0.6,0.3,0.3] ,
#                   [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,0.0,0.0] ,
#                   [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,0.0] ]



def determineTable(options):
    if (options.edgeTable == "actual"):
        if (options.classes == "2"):
            tbl = tblMMActual_2class
        elif (options.classes == "3"):
            tbl = tblMMActual_3class

    elif (options.edgeTable == "potential"):
        if (options.classes == "2"):
            tbl = tblMMPotential_2class
        elif (options.classes == "3"):
            tbl = tblMMPotential_3class

    return tbl

'''
La decision de si hay arista o no se basara en los tipos de los nodos y los 
coeficientes de las tablitas.
'''

def edgeExistsW0Biotic(srcClass, dstClass, options):

    tbl = determineTable(options)


    print "src,dst classes:" , srcClass, dstClass
    if (srcClass < dstClass):
        coeff = tbl[srcClass-1][dstClass-1]
    else:
        coeff = tbl[dstClass-1][srcClass-1]

    return (coeff >= 0.5)


def edgeExistsW0BioticAndVirulence(srcClass, dstClass, srcVirulence, destPatches, options):

    tbl = determineTable(options)

    if (srcClass < dstClass):
        coeff = tbl[srcClass-1][dstClass-1]
    else:
        coeff = tbl[dstClass-1][srcClass-1]

    if (destPatches > 0 and srcVirulence < 2):
        coeff = 0

    return (coeff >= 0.5)


def edgeExistsW0BioticWSpeciesProb(srcClass, dstClass, destPatches, 
    destSpeciesProbMimic, destSpeciesProbPupha, options ):

    print "src,dst classes:" , srcClass, dstClass

    tbl = determineTable(options)


    if (srcClass < dstClass):
        coeff = tbl[srcClass-1][dstClass-1]
    else:
        coeff = tbl[dstClass-1][srcClass-1]

    if (destSpeciesProbPupha >= destSpeciesProbMimic  and destPatches > 0):
        coeff = 0
 
    return (coeff >= 0.5)

def edgeExistsW0BioticWSpeciesProbAndVirulence(srcClass, dstClass, destPatches, 
    destSpeciesProbMimic, destSpeciesProbPupha, srcVirulence, options ):

    print "src,dst classes:" , srcClass, dstClass

    tbl = determineTable(options)


    if (srcClass < dstClass):
        coeff = tbl[srcClass-1][dstClass-1]
    else:
        coeff = tbl[dstClass-1][srcClass-1]


    if (destSpeciesProbPupha >= destSpeciesProbMimic  or ( srcVirulence < 2 and destPatches > 0)):
        coeff = 0

    return (coeff >= 0.5)


'''
La decision de si hay arista o no se basara en los tipos de los nodos Y 
la densidad de parchos.
'''
def edgeExistsW0BioticWPatchDensity(srcClass, dstClass, srcDens, dstDens):

    print
    print "src,dst classes:" , srcClass, dstClass
    # if (srcClass < dstClass):
    #     coeff = tblW0Biotic[srcClass-1][dstClass-1]
    # else:
    #     coeff = tblW0Biotic[dstClass-1][srcClass-1]

    # print "srcClass, dstClass, srcDens, dstDens:" ,srcClass, dstClass, srcDens, dstDens

    # coeff = coeff * (srcDens + dstDens + 1)


    # return (coeff >= 0.5)




'''
Given the global bounding rectangle, inserts the items into a quadTree 
'''
def createAndInsertAll(globalBBox):
    items = []
    qTree = pyqtree.Index(bbox=globalBBox) 
    for i in range(0, 200): #inLayer.GetFeatureCount()):
        fI = inLayer.GetFeature(i)
        fIGeom = fI.GetGeometryRef()
        fIBBox = fIGeom.GetEnvelope()
        item = Node(fI.GetField("ID"), 
            [fIBBox[0], fIBBox[2], fIBBox[1], fIBBox[3]]) 
        items.append(item)

    random.shuffle(items)

    i = 0
    for item in items:
        qTree.insert(item=item, bbox=item.bbox)
        print "Inserting %d" % item.id
        print i, qTree.countmembers()
        i = i + 1

'''
Creates the output layer and data source. Also creates the fields.
'''
def createOutputLayer(myOutDriver, myOutShapeFile):
    myOutDataSource = myOutDriver.CreateDataSource(myOutShapeFile)
    myOutLayer      = myOutDataSource.CreateLayer(myOutShapeFile, 
                            geom_type=ogr.wkbLineString)

    outFields = [["SourceID", ogr.OFTInteger],
                 ["DestID",   ogr.OFTInteger],
                 ["SourceX",  ogr.OFTReal],
                 ["SourceY",  ogr.OFTReal],
                 ["DestX",    ogr.OFTReal],
                 ["DestY",    ogr.OFTReal],
                 ["SrcClass",ogr.OFTInteger],
                 ["DstClass",  ogr.OFTInteger] ] #,
                 #["GRIDCODE",  ogr.OFTInteger],
                 #["mimicProb",  ogr.OFTReal],
                 #["patches",  ogr.OFTInteger]]

    for f in outFields:
        aNewField = ogr.FieldDefn(f[0], f[1] )
        print "Creating output Column: %s" % f[0]
        myOutLayer.CreateField(aNewField)    

    return myOutDataSource, myOutLayer

'''
Given an input layer create an output layer of vertices for every two
nodes that "touch" (share boundaries)
'''
def intersectLayers(habitatLayer, parchoLayer, myOutLayer):
    myOutLayerDefn = myOutLayer.GetLayerDefn()
    


    # graph = pydot.Dot(graph_type='graph')


    for i in range(0,  habitatLayer.GetFeatureCount()):
        # get the feature and its geometry
        fI = habitatLayer.GetFeature(i)
        fIName = fI.GetField("ID")
        fIGeom = fI.GetGeometryRef()
        fIBBox = fIGeom.GetEnvelope()


        habitatMinX, habitatMaxX, habitatMinY, habitatMaxY = fIBBox
        print i, habitatMinX, habitatMaxX, habitatMinY, habitatMaxY, float(i*100)/habitatLayer.GetFeatureCount()
        # inFeature = habitatLayer.GetFeature(i)
        # outFeature = ogr.Feature(myOutLayerDefn)

        # hits = myGrid.hits(i)
        # print "possible hits:", len(hits)
        # remHits = set([])
        # for e in list(hits):
        #     if int(e) <= i: 
        #         remHits.add(e)
        # hits = hits.difference(remHits)

        for j in range(0, parchoLayer.GetFeatureCount()): 
            # get the feature and its geometry
            fJ = parchoLayer.GetFeature(j)
            fJName = fJ.GetField("patchID")
            fJGeom = fJ.GetGeometryRef()
            fJBBox = fJGeom.GetEnvelope()

            parchoMinX, parchoMaxX, parchoMinY, parchoMaxY = fJBBox

            # fJPatchDensity = fJ.GetField("DensPatch")


            # fJPatches =      fJ.GetField("patches")
            # fJSpeciesProbMimic =      fJ.GetField("mimicProb")
            # fJSpeciesProbPupha =      fJ.GetField("puphaProb")

            # srcNodeClass = nodeProperties[i]
            

            # print "COmparing BBox:", fIBBox, " to BBox ", parchoMinX, parchoMaxX, parchoMinY, parchoMaxY
            if (habitatMinX < parchoMaxX and habitatMaxX > parchoMinX and habitatMinY < parchoMaxY and habitatMaxY > parchoMinY):
              print "possible intersect:", i, j


def joinByBoundary(myInLayer, myOutLayer, myDistance, myGrid, options, myDotOutFile = None):
    myOutLayerDefn = myOutLayer.GetLayerDefn()
    
    if myDistance == None: myDistance = -1
    else: myDistance = int(myDistance)

    # graph = pydot.Dot(graph_type='graph')


    for i in range(0, myInLayer.GetFeatureCount()):
        # get the feature and its geometry
        fI = myInLayer.GetFeature(i)
        fIName = fI.GetField("ID")
        fIGeom = fI.GetGeometryRef()
        fIBBox = fIGeom.GetEnvelope()
        if options.edgeMethod == "BP":
            fIPatchDensity = fI.GetField("patchDens")
        # fIPatchDensity = fI.GetField("DensPatch")
        if options.edgeMethod == "BV" or options.edgeMethod == "BSV":
            srcVirulence = fI.GetField("Virulence")

        inFeature = myInLayer.GetFeature(i)
        outFeature = ogr.Feature(myOutLayerDefn)

        hits = myGrid.hits(i)
        print "possible hits:", len(hits)
        remHits = set([])
        for e in list(hits):
            if int(e) <= i: 
                remHits.add(e)
        hits = hits.difference(remHits)

        for j in hits: #range(i+1, inLayer.GetFeatureCount()): 
            # get the feature and its geometry
            fJ = myInLayer.GetFeature(j)
            fJName = fJ.GetField("ID")
            fJGeom = fJ.GetGeometryRef()
            # fJPatchDensity = fJ.GetField("DensPatch")
            if options.edgeMethod == "BP":
                fJPatchDensity = fJ.GetField("patchDens")

            fJPatches =      fJ.GetField("patches")
            fJSpeciesProbMimic =      fJ.GetField("mimicProb")
            fJSpeciesProbPupha =      fJ.GetField("puphaProb")

            srcNodeClass = nodeProperties[i]
            
            # print srcNodeClass

            # if the distance was None, then we join the nodes that are "touching

            # fIGeom.Centroid().Distance(fJGeom.Centroid())
            if ((myDistance >= 0 and fIGeom.Distance(fJGeom) < int(myDistance)) \
                or (myDistance == -1 and fI.GetGeometryRef().Touches(fJ.GetGeometryRef()) ) ):
        
                print fIName, "touches", fJName,

                dstNodeClass = nodeProperties[j]

                edgeExists = False

                if options.edgeMethod == "B":
                    edgeExists = edgeExistsW0Biotic(srcNodeClass, dstNodeClass, options)
                elif options.edgeMethod == "BP":
                    edgeExists = edgeExistsW0BioticWPatchDensity(srcNodeClass, dstNodeClass, fIPatchDensity, fJPatchDensity)
                elif options.edgeMethod == "BS":
                    edgeExists = edgeExistsW0BioticWSpeciesProb(srcNodeClass, dstNodeClass, fJPatches, fJSpeciesProbMimic, fJSpeciesProbPupha, options)
                elif options.edgeMethod == "BV":
                    edgeExists = edgeExistsW0BioticAndVirulence(srcNodeClass, dstNodeClass, srcVirulence, fJPatches, options)
                elif options.edgeMethod == "BSV":
                    edgeExists = edgeExistsW0BioticWSpeciesProbAndVirulence(srcNodeClass, dstNodeClass, fJPatches, fJSpeciesProbMimic, fJSpeciesProbPupha, srcVirulence, options)


                if (edgeExists):
                # if edgeExistsW0BioticWSpeciesProb(srcNodeClass, dstNodeClass, fJPatches, fJSpeciesProbMimic, fJSpeciesProbPupha):
                # if(edgeExistsW0BioticWPatchDensity(srcNodeClass, dstNodeClass, fIPatchDensity, fJPatchDensity)):
                # if edgeExistsW0Biotic(srcNodeClass, dstNodeClass):

                    #print " Distance: ", fIGeom.Distance(fJGeom), 
                    #print " Centroid Distance: ", fIGeom.Centroid().Distance(fJGeom.Centroid())
                    
                    # create a line from centroid of city i to city j
                    multiline = ogr.Geometry(ogr.wkbLineString)
                    multiline.AddPoint(fIGeom.Centroid().GetX(),fIGeom.Centroid().GetY())
                    multiline.AddPoint(fJGeom.Centroid().GetX(),fJGeom.Centroid().GetY())

                    # add the multiline as a feature to the output layer
                    outFeature = ogr.Feature(outLayerDefn)            
                    outFeature.SetGeometry(multiline)



                    # add the rest of the features to the line
                    outFeature.SetField("SourceID" , fIName)
                    outFeature.SetField("DestID"   , fJName)
                    outFeature.SetField("SourceY"  , fIGeom.Centroid().GetY())
                    outFeature.SetField("SourceX"  , fIGeom.Centroid().GetX())
                    outFeature.SetField("SourceY"  , fIGeom.Centroid().GetY())
                    outFeature.SetField("DestX"    , fJGeom.Centroid().GetX())
                    outFeature.SetField("DestY"    , fJGeom.Centroid().GetY())
                    outFeature.SetField("SrcClass", nodeProperties[i])
                    outFeature.SetField("DstClass", nodeProperties[j])
                    #outFeature.SetField("GRIDCODE", nodeProperties[j])

                    outLayer.CreateFeature(outFeature)

                    # edge = pydot.Edge(fIName, fJName)
                    # graph.add_edge(edge)

    # f = open(myDotOutFile, 'w')
    # f.write(graph.to_string())
    # f.close()


def defineCLOptions():
    parser = OptionParser()
    parser.add_option("-s", "--habitatShapeFile")
    parser.add_option("-p", "--parchoShapeFile")
    parser.add_option("-o", "--outputFile")

    return parser.parse_args()

def validArguments(myOptions):
    if myOptions.habitatShapeFile == None:
        print "Please specify habitat shape file!"
        return False
    
    if not(os.path.isfile(myOptions.habitatShapeFile)):
        print "No such habitat file exists (%s)" % myOptions.habitatShapeFile
        return False

    if myOptions.parchoShapeFile == None:
        print "Please specify parcho shape file!"
        return False
    
    if not(os.path.isfile(myOptions.parchoShapeFile)):
        print "No such habitat file exists (%s)" % myOptions.parchoShapeFile
        return False


    if myOptions.outputFile == None:
        print "Please specify output file!"
        return False

    return True

if __name__ == "__main__":
    # Gather the command line parameters
    # inShapefile = "../data/LIMITES_LEGALES_MUNICIPIOS_EDICION_MARZO2009/LIMITES_LEGALES_MUNICIPIOS_EDICION_MARZO2009.shp"
    # outShapefile = "/tmp/test.shp"


    (options, args) = defineCLOptions()

    if not(validArguments(options)): sys.exit(1)

    # Get the input Layer
    inDriver     = ogr.GetDriverByName("ESRI Shapefile")
    habitatDataSource = inDriver.Open(options.habitatShapeFile, 0)
    habitatLayer      = habitatDataSource.GetLayer()

    parchoDataSource = inDriver.Open(options.parchoShapeFile, 0)
    parchoLayer      = parchoDataSource.GetLayer()

    # Create the output Layer
    outDriver    = ogr.GetDriverByName("ESRI Shapefile")

    # Remove output shapefile if it already exists
    if os.path.exists(options.outputFile):
        outDriver.DeleteDataSource(options.outputFile)

    outDataSource, outLayer = createOutputLayer(outDriver,options.outputFile)

    # Get the output Layer's Feature Definition
    outLayerDefn = outLayer.GetLayerDefn()

    intersectLayers(habitatLayer, parchoLayer, outLayer)
    exit(1)

    globalBBox = getGlobalBBox(habitatLayer, parchoLayer)
    g = grid.Grid(globalBBox,1000)

    insertAllToGrid(g,options)

    print nodeProperties


    startTime = time.time()

    joinByBoundary(inLayer, outLayer, options.distance,  g, options) #, options.outputDotFile)
    #joinByDistance(inLayer, outLayer)

    print "..... done in ", time.time() - startTime


    # Close DataSources
    inDataSource.Destroy()
    outDataSource.Destroy()
