import sys
sys.path = ['/Applications/QGIS.app/Contents/MacOS/../Resources/python/plugins/processing', '/Applications/QGIS.app/Contents/MacOS/../Resources/python', u'/Users/rarce/.qgis2/python', u'/Users/rarce/.qgis2/python/plugins', '/Applications/QGIS.app/Contents/MacOS/../Resources/python/plugins', '/Library/Frameworks/SQLite3.framework/Versions/C/Python/2.7', '/Library/Frameworks/GEOS.framework/Versions/3/Python/2.7', '/Library/Python/2.7/site-packages/matplotlib-override', '/Library/Frameworks/GDAL.framework/Versions/1.11/Python/2.7/site-packages', '/Library/Python/2.7/site-packages/virtualenv-1.8.2-py2.7.egg', '/Library/Python/2.7/site-packages/virtualenvwrapper-3.6-py2.7.egg', '/Library/Python/2.7/site-packages/stevedore-0.4-py2.7.egg', '/Library/Python/2.7/site-packages/virtualenv_clone-0.2.4-py2.7.egg', '/Library/Python/2.7/site-packages/distribute-0.6.28-py2.7.egg', '/Library/Python/2.7/site-packages/pip-1.2.1-py2.7.egg', '/Library/Python/2.7/site-packages/Flask-0.9-py2.7.egg', '/Library/Python/2.7/site-packages/Jinja2-2.6-py2.7.egg', '/Library/Python/2.7/site-packages/Werkzeug-0.8.3-py2.7.egg', '/Library/Python/2.7/site-packages/pydot-1.0.28-py2.7.egg', '/Library/Python/2.7/site-packages/python_graph_core-1.8.2-py2.7.egg', '/Library/Python/2.7/site-packages/python_graph_dot-1.8.2-py2.7.egg', '/Library/Frameworks/cairo.framework/Versions/1/Python/2.7', '/System/Library/Frameworks/Python.framework/Versions/2.7/lib/python27.zip', '/System/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7', '/System/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/plat-darwin', '/System/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/plat-mac', '/System/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/plat-mac/lib-scriptpackages', '/System/Library/Frameworks/Python.framework/Versions/2.7/Extras/lib/python', '/System/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/lib-tk', '/System/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/lib-old', '/System/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/lib-dynload', '/System/Library/Frameworks/Python.framework/Versions/2.7/Extras/lib/python/PyObjC', '/Library/Python/2.7/site-packages', '/Library/Python/2.7/site-packages/setuptools-0.6c11-py2.7.egg-info', '/Applications/QGIS.app/Contents/Resources/python/plugins/fTools/tools']


import random
from osgeo import gdal, ogr    

RASTERIZE_COLOR_FIELD = "__color__"

def rasterize(pixel_size=2500):
    # Open the data source
    orig_data_source = ogr.Open("/Users/rarce/Downloads/qgis_sample_data/shapefiles/lakes.shp")
    print orig_data_source
    
    # Make a copy of the layer's data source because we'll need to 
    # modify its attributes table
    source_ds = ogr.GetDriverByName("Memory").CopyDataSource(orig_data_source, "")
    source_layer = source_ds.GetLayer(0)
    source_srs = source_layer.GetSpatialRef()
    x_min, x_max, y_min, y_max = source_layer.GetExtent()

    # Create a field in the source layer to hold the features colors
    field_def = ogr.FieldDefn(RASTERIZE_COLOR_FIELD, ogr.OFTReal)
    source_layer.CreateField(field_def)
    source_layer_def = source_layer.GetLayerDefn()
    field_index = source_layer_def.GetFieldIndex(RASTERIZE_COLOR_FIELD)
    
    # Generate random values for the color field (it's here that the value
    # of the attribute should be used, but you get the idea)
    for feature in source_layer:
        feature.SetField(field_index, random.randint(0, 255))
        source_layer.SetFeature(feature)

    # Create the destination data source
    x_res = int((x_max - x_min) / pixel_size)
    y_res = int((y_max - y_min) / pixel_size)
    target_ds = gdal.GetDriverByName('GTiff').Create('test.tif', x_res,
            y_res, 3, gdal.GDT_Byte)
    target_ds.SetGeoTransform((
            x_min, pixel_size, 0,
            y_max, 0, -pixel_size,
        ))

    if source_srs:
        # Make the target raster have the same projection as the source
        target_ds.SetProjection(source_srs.ExportToWkt())
    else:
        # Source has no projection (needs GDAL >= 1.7.0 to work)
        target_ds.SetProjection('LOCAL_CS["arbitrary"]')

    # Rasterize
    err = gdal.RasterizeLayer(target_ds, (3, 2, 1), source_layer,
            burn_values=(0, 0, 0),
            options=["ATTRIBUTE=%s" % RASTERIZE_COLOR_FIELD])
    if err != 0:
        raise Exception("error rasterizing layer: %s" % err)

def centroidize(pixel_size=2500):
    # Open the data source
    orig_data_source = ogr.Open("/Users/rarce/Downloads/qgis_sample_data/shapefiles/lakes.shp")
    source_ds = ogr.GetDriverByName("Memory").CopyDataSource(orig_data_source, "")
    print source_ds
    for layer in source_ds:
        print layer.GetName()
        print "Count:" ,layer.GetFeatureCount()
        for feature in layer:
            print feature
            pt =  feature.GetGeometryRef().Centroid().ExportToWkt().replace("(","").replace(")","").split(" ")
            point = ogr.Geometry(ogr.wkbPoint)
            point.AddPoint(float(pt[1]),float(pt[2]))
            print point.ExportToWkt()



        # feat = QgsFeature()
        # feat.addAttribute(0, 'hello')
        # feat.setGeometry(QgsGeometry.fromPoint(QgsPoint(123, 456)))
        # (res, outFeats) = layer.dataProvider().addFeatures([feat])

    source_layer = source_ds.GetLayer(0)

    feat = ogr.Feature(ogr.FeatureDefn("caca"))
    feat.SetGeometry(point)
    source_layer.CreateFeature(feat)
    print "Count:" ,layer.GetFeatureCount()


    source_srs = source_layer.GetSpatialRef()
    x_min, x_max, y_min, y_max = source_layer.GetExtent()

    # Create a field in the source layer to hold the features colors
    field_def = ogr.FieldDefn(RASTERIZE_COLOR_FIELD, ogr.OFTReal)
    source_layer.CreateField(field_def)
    source_layer_def = source_layer.GetLayerDefn()
    field_index = source_layer_def.GetFieldIndex(RASTERIZE_COLOR_FIELD)
    
    # Generate random values for the color field (it's here that the value
    # of the attribute should be used, but you get the idea)
    for feature in source_layer:
        feature.SetField(field_index, random.randint(0, 255))
        source_layer.SetFeature(feature)

    # Create the destination data source
    x_res = int((x_max - x_min) / pixel_size)
    y_res = int((y_max - y_min) / pixel_size)
    target_ds = gdal.GetDriverByName('GTiff').Create('test.tif', x_res,
            y_res, 3, gdal.GDT_Byte)
    target_ds.SetGeoTransform((
            x_min, pixel_size, 0,
            y_max, 0, -pixel_size,
        ))

    if source_srs:
        # Make the target raster have the same projection as the source
        target_ds.SetProjection(source_srs.ExportToWkt())
    else:
        # Source has no projection (needs GDAL >= 1.7.0 to work)
        target_ds.SetProjection('LOCAL_CS["arbitrary"]')

    # Rasterize
    err = gdal.RasterizeLayer(target_ds, (3, 2, 1), source_layer,
            burn_values=(0, 0, 0),
            options=["ATTRIBUTE=%s" % RASTERIZE_COLOR_FIELD])
    if err != 0:
        raise Exception("error rasterizing layer: %s" % err)

    outShapefile = "lakesMod.shp"
    outDriver = ogr.GetDriverByName("ESRI Shapefile")
    outDataSource = outDriver.CreateDataSource(outShapefile)
    outLayer = outDataSource.CreateLayer("states_extent", geom_type=ogr.wkbPolygon)

centroidize()

#rasterize()