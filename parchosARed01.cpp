#include <gdal_priv.h>
#include <cpl_conv.h> // for CPLMalloc()
#include <string>
#include <ogrsf_frmts.h>
#include <gdal_priv.h>
#include <iostream>
#include <gdal.h>
#include <vector>
#include <map>
#include <cmath>
#include <iomanip>
#include <set>
#include <ogr_geometry.h> 
#include <iostream>
#include <deque>

#include "coefficients.h"
#include "polysplit.h"
#include "edge.h"

// Given the susceptibility class, the probability and presence of patches
// assigns a class for the node


int classifyNode_TwoClasses(int susClass, double speciesProb, int patchesPresent){
    int nodeClass;
    
    std::cout << "susclass, speciedProb, patches: " << susClass << " " << speciesProb << " " << patchesPresent << std::endl;
    if (susClass == 2) {
        if (speciesProb >= 0.5)     nodeClass = (patchesPresent > 0) ? 1 : 2;
        else                        nodeClass = (patchesPresent > 0) ? 3 : 4;
    }
    else {
        if (speciesProb >= 0.5)     nodeClass = (patchesPresent > 0) ? 5 : 6;
        else                        nodeClass = (patchesPresent > 0) ? 7 : 8;
    }
    return nodeClass;
}

int classifyNode_ThreeClasses(int susClass, double speciesProb, int patchesPresent){
    int nodeClass;
    
    if (susClass == 3) {
        if (speciesProb >= 0.5) nodeClass = (patchesPresent > 0) ? 1 : 2;
        else                    nodeClass = (patchesPresent > 0) ? 3 : 4;
    }
    else if (susClass == 2) {
        if (speciesProb >= 0.5) nodeClass = (patchesPresent > 0) ? 5 : 6;
        else                    nodeClass = (patchesPresent > 0) ? 7 : 8;
    }
    else {
        if (speciesProb >= 0.5) nodeClass = (patchesPresent > 0) ? 9 : 10;
        else                    nodeClass = (patchesPresent > 0) ? 11 : 12;
    }
    return nodeClass;
}

float ** determineTable(const std::string &edgeTable,  int numClasses){
    float **tbl = NULL;
    if (edgeTable == "actual") {
        if (numClasses == 2)        tbl = MMActual_2class;
        else if (numClasses == 3)   tbl = MMActual_3class;
    }
    else if (edgeTable == "potential") {
        if (numClasses == 2)           tbl = MMPotential_2class;
        else if (numClasses == 3)      tbl = MMPotential_3class;
    }
    return tbl;
}







OGRLayer* createOutputLayer(OGRSFDriver *outDriver, std::string outFN, OGRDataSource *outDataSource) {
    OGRLayer *outLayer;
   
    outLayer  = outDataSource->CreateLayer(outFN.c_str(),NULL, wkbLineString);
    if (outLayer == NULL) std::cout << "outLayer is NULL!!!!\n";  
    
    // create the fields for the output file
    OGRFieldDefn fieldDefn("SourceID", OFTInteger);
    outLayer->CreateField(&fieldDefn);
    fieldDefn.SetName("DestID");
    outLayer->CreateField(&fieldDefn);
    fieldDefn.SetName("SourceX"); fieldDefn.SetType(OFTReal); outLayer->CreateField(&fieldDefn);
    fieldDefn.SetName("SourceY"); fieldDefn.SetType(OFTReal); outLayer->CreateField(&fieldDefn);
    fieldDefn.SetName("DestX"); fieldDefn.SetType(OFTReal); outLayer->CreateField(&fieldDefn);
    fieldDefn.SetName("DestY"); fieldDefn.SetType(OFTReal); outLayer->CreateField(&fieldDefn);
    fieldDefn.SetName("SrcClass"); fieldDefn.SetType(OFTInteger); outLayer->CreateField(&fieldDefn);
    fieldDefn.SetName("DstClass"); fieldDefn.SetType(OFTInteger); outLayer->CreateField(&fieldDefn);
    
    if (outLayer == NULL) { 
        std::cout << "outLayer is NULL!!!!\n";  
        exit(1);
    }
    
    return outLayer;
}

//
// This function I adapted from polysplit.cpp written by Schuyler Erle <schuyler@nocat.net>
// Given a layer splits all the polygons so that they do not have more than a given MAXVERTICES
//

void splitLayerGeometries(OGRLayer *srcLayer, OGRPolyList &pieces, std::vector<int> &pieceToID){
    OGRFeature *feature;
    
    int features_read = 0, features_written = 0,
            total = srcLayer->GetFeatureCount();
    
    srcLayer->ResetReading();
    int id_field = -1;
    while( (feature = srcLayer->GetNextFeature()) != NULL ) {
        /* Make an empty parts list, and get the ID and geometry from the input. */
        OGRPolyList tmpPieces;
        int max_vertices = MAXVERTICES;
        
        feature_id_t id = (id_field >= 0 ? feature->GetFieldAsInteger("ID") //id_field)
                                         : feature->GetFID());
        OGRGeometry *geometry = feature->GetGeometryRef();
        
        /* Recursively split the geometry, and write a new feature for each polygon that comes out. */
        split_polygons(&tmpPieces, geometry, max_vertices);
        for (OGRPolyList::iterator it = tmpPieces.begin(); it != tmpPieces.end(); it++) {
            
            if(debug) std::cout << "A polygon for id: " << id << std::endl;
            if(debug) std::cout << "Area : "  << static_cast<OGRSurface*>(*it)->get_Area() << std::endl;
            pieceToID.push_back(id);
            pieces.push_back(*it);
        }
        
        features_read++;
        if (debug) std::cerr << features_read << " / " << total << "\r";            

        OGRFeature::DestroyFeature( feature );
    }
    std::cout << "Total of polygons after split:" << features_written << "\n";
}

//
// Goes through all the habitats and assigns a class, given their GRIDCODE, 
// mimicPron and number of patches. Updates the habitatClass vector.
//

void classifyAllHabitats( OGRLayer *habitatLayer, int numClasses, std::vector <int> &habitatClass) {
    OGRFeature *feature;
    habitatLayer->ResetReading();
    habitatClass.resize(habitatLayer->GetFeatureCount());
    int id = 0, tmpClass;
    while( (feature = habitatLayer->GetNextFeature()) != NULL ) {

        int gridCode = feature->GetFieldAsInteger("GRIDCODE");
        double mimicProb = feature->GetFieldAsDouble("mimicProb");
        int patches = feature->GetFieldAsInteger("patches");
        
        if (numClasses == 2)
            tmpClass = classifyNode_TwoClasses(gridCode, mimicProb, patches); 
        else if (numClasses == 3)
            tmpClass = classifyNode_ThreeClasses(gridCode, mimicProb, patches);  
        habitatClass[id] = tmpClass;
        
        if (debug) std::cout << id << " classified as " << tmpClass << std::endl; 
        id++;       
    }
}

void usage(void) {
    std::cerr << "\nUsage: polysplit [opts] <input> <output>\n\n"
              << "\t-i\tinput layer name\n"
              << "\t-o\toutput layer name\n"
              << "\t-f\tOGR output driver name\n"
              << "\t-n\tID field name (must be integer type)\n"
              << "\t-m\tMax vertices per output polygon\n"
              << "\t-v\tVerbose mode\n\n";
    exit(1);
}

int main(int argc, char** argv) {
    
    initTables();
    
    int opt, numClasses = 0;
    std::string patchFN, habitatFN, outputFN, edgeMethod, edgeTable;
    
    while ((opt = getopt(argc, argv, "p:h:o:c:e:t:v")) != -1) {
        switch (opt) {
        case 'p': patchFN   = optarg;     break;
        case 'h': habitatFN = optarg;     break;
        case 'o': outputFN  = optarg;     break;
        case 'c': numClasses  = atoi(optarg);     break;
        case 'e': edgeMethod = optarg;     break;
        case 't': edgeTable = optarg;     break;
            // case 'f': driver_name = optarg;         break;
            // case 'n': id_field_name = optarg;       break;
            // case 'm': max_vertices = atoi(optarg);  break;
        case 'v': debug = true;                 break;
        default: usage();
        }
    }
    argc -= optind;
    argv += optind;
    
    if (patchFN.length() == 0 || habitatFN.length() == 0 ) {
        std::cout << "Please provide the name of the habitat and patch shapefiles!!!" << std::endl;
        return 1;
    }
    else if (numClasses < 2 || numClasses > 3 ) {
        std::cout << "Please provide the number of classes!!!" << std::endl;
        return 1;
    }    
    else if (edgeMethod.length() == 0 || edgeTable.length() == 0 ) {
        std::cout << "Please provide edge method and table!!!" << std::endl;
        return 1;
    }
    else if (edgeMethod != "B" && edgeMethod != "BS" && edgeMethod != "BSV" && edgeMethod != "BV") {
        std::cout << "Please specify a valid edgeMethod: B, BS, BSV or BV" << std::endl;
        exit(1);        
    } 
    else if (edgeTable != "potential" && edgeTable != "actual") {
        std::cout << "Please specify a valid edgeTable: actual or potential" << std::endl;
        exit(1);        
    } 


    
    OGRRegisterAll();
    OGRLayer *myLayer, *habitatLayer;
    OGRDataSource *hDS, *habitatDataSource;
    OGRSFDriver *driver;
    OGRSFDriverRegistrar *registrar =  OGRSFDriverRegistrar::GetRegistrar();
    OGREnvelope *habitatBBox = new OGREnvelope;
    OGREnvelope *jhabitatBBox = new OGREnvelope;
        
    std::vector <OGRFeature *> VF;
    std::vector <OGREnvelope *> habitatSplitBBox;
    std::vector <OGRGeometry *> VGeom;
    std::vector <double> patchAreaVec, habitatAreaVec;
    
    
    // open the files
    driver = registrar->GetDriverByName("ESRI Shapefile");
    hDS = driver->Open(patchFN.c_str(),0);
    habitatDataSource = driver->Open(habitatFN.c_str(),true);
    
    
    std::cout << hDS->GetLayerCount() << std::endl;;
    myLayer = hDS->GetLayer(0);
    habitatLayer = habitatDataSource->GetLayer(0);
    
    OGRDataSource *outDataSource;
    OGRLayer *outLayer;
    
    
    outDataSource = driver->CreateDataSource(outputFN.c_str(), NULL);
    if (outDataSource == NULL) std::cout << "outDataSource is NULL!!!!\n";  
    outLayer = createOutputLayer(driver, outputFN, outDataSource);
    
    // if (outLayer == NULL) std::cout << "back in main out layer is null......\n";
    OGRFeatureDefn *outLayerDefn = outLayer->GetLayerDefn();

    outDataSource->SyncToDisk ();
    
    // Classify all the habitats, update their entry in the habitatClass vector
    std::vector <int> habitatClass;
    std::cout << "Classifying all habitats.......\n";
    classifyAllHabitats(habitatLayer, numClasses, habitatClass );
    
    // Assign a coefficient table based on the given parameter    
    float **tbl = determineTable(edgeTable, numClasses);
    std::cout << "Table chosen...." << std::endl;
    print(tbl, 8);

    
    // Let's split the polygons
     
    OGRPolyList pieces;  // this where the split polygons will be stored

    // The index of this vector the split polygon index, the content is the original ID
    std::vector <int> pieceToID;
    splitLayerGeometries(habitatLayer, pieces, pieceToID);
    std::cout << "After split, number of polygons: " << pieces.size()<< std::endl;
    
    
    // Update the vector that keeps track of the bounding box of the split polygons
    for (OGRPolyList::iterator it = pieces.begin(); it != pieces.end(); it++) {
        OGREnvelope *tmp = new OGREnvelope;
        static_cast<OGRSurface*>(*it)->getEnvelope(tmp);
        habitatSplitBBox.push_back(tmp);
    }
        
    
    // This is the structure where we will store the habitats that are touched 
    // by a given habitat.
    std::vector < std::set< int > > VTouchedHabitats; 
    VTouchedHabitats.resize(habitatLayer->GetFeatureCount()+1);

  
    std::cout << "Computing neighboor habitats......" << std::endl;
    int pieceCtr = 0;
    int featureID;
    for (OGRPolyList::iterator it = pieces.begin(); it != pieces.end(); it++) {
        
        featureID = pieceToID[pieceCtr]; 
        static_cast<OGRSurface*>(*it)->getEnvelope(habitatBBox); 
        
        int jCtr = pieceCtr+1;
        
        for (OGRPolyList::iterator jit = it + 1;  jit != pieces.end(); jit++) {
            int featureJ =  pieceToID[jCtr];  // = myLayer->GetFeature(j)
            jhabitatBBox = habitatSplitBBox[jCtr];
            
            if (habitatBBox->MinX < jhabitatBBox->MaxX && 
                    habitatBBox->MaxX > jhabitatBBox->MinX &&
                    habitatBBox->MinY < jhabitatBBox->MaxY && 
                    habitatBBox->MaxY > jhabitatBBox->MinY) {
                
                if ( static_cast<OGRGeometry*>(*jit)->Touches(static_cast<OGRGeometry*>(*it)) ) {
                    if (debug) std::cout  << featureID << " Touches: " <<  featureJ << std::endl;
                    VTouchedHabitats[featureID].insert(featureJ);
                    //VTouchedHabitats[featureJ].insert(featureID);
                }
                
            }
            jCtr++;
        }   
        pieceCtr++;
    }


    // Next, we will filter the edges based on certain rules

    int srcID, srcClass;
    double sourceX, sourceY;
    int edgeCtr = 0;
    
    std::cout << "Storing edges..... " << std::endl;

    // Print the patches that linked to every node ...
    for (int i = 0; i < VTouchedHabitats.size() - 1; i++) {
        if(debug) std::cout  << i << " Touches: " << std::endl;
        
        OGRFeature *iFeature = habitatLayer->GetFeature(i);
        OGRPoint pointSrc;        
        iFeature->GetGeometryRef()->Centroid(&pointSrc);
        
        srcID =  iFeature->GetFieldAsInteger("ID");
        sourceX =  pointSrc.getX();
        sourceY =  pointSrc.getY();

        srcClass = habitatClass[i];
        
        
        // double srcPatchDensity = iFeature->GetFieldAsDouble("patchDens");
        double srcVirulence;
        if (edgeMethod == "BSV" || edgeMethod == "BS") 
            srcVirulence = iFeature->GetFieldAsDouble("Virulence");

        for (std::set<int>::iterator sit = VTouchedHabitats[i].begin(); 
             sit != VTouchedHabitats[i].end(); sit++) { 
            if (debug) std::cout << "(" << *sit << ")" << std::endl;
            
            
            OGRFeature *jFeature = habitatLayer->GetFeature(*sit);
            
            bool edgeExists = false;
            int dstClass = habitatClass[*sit];

            float dstPatches = jFeature->GetFieldAsDouble("patches");
            float destSpeciesProbMimic = jFeature->GetFieldAsDouble("mimicProb");
            float destSpeciesProbPupha = jFeature->GetFieldAsDouble("puphaProb");
            
            // float destPatchDensity = jFeature->GetFieldAsDouble("patchDens");

            if (edgeMethod == "B")
                edgeExists = edgeExistsW0Biotic(srcClass, dstClass, tbl);
            // else if (edgeMethod == "BP")
            //     edgeExists = edgeExistsW0BioticWPatchDensity(srcClass, dstClass, srcPatchDensity, destPatchDensity, edgeTable, numClasses);
            else if (edgeMethod == "BS")
                edgeExists = edgeExistsW0BioticWSpeciesProb(srcClass, dstClass, dstPatches, destSpeciesProbMimic, destSpeciesProbPupha, tbl);
            else if (edgeMethod == "BV")
                edgeExists = edgeExistsW0BioticAndVirulence(srcClass, dstClass, srcVirulence, dstPatches, tbl);
            else if (edgeMethod == "BSV")
                edgeExists = edgeExistsW0BioticWSpeciesProbAndVirulence(srcClass, dstClass, dstPatches, destSpeciesProbMimic, destSpeciesProbPupha, srcVirulence, tbl);
            
            if(debug) std::cout << "edge exists: " << edgeExists << std::endl;

            if (edgeExists) {
                
                OGRLineString *multiline = new OGRLineString();
                OGRPoint pointDest;
                jFeature->GetGeometryRef()->Centroid(&pointDest);
                
                multiline->addPoint(&pointSrc);
                multiline->addPoint(&pointDest);
                
                
                OGRFeature tmpFeature(outLayerDefn);
                tmpFeature.SetGeometry(multiline);
                
                //add the rest of the features to the line
                tmpFeature.SetField("SourceID" , srcID);
                tmpFeature.SetField("DestID" , jFeature->GetFieldAsInteger("ID"));
                tmpFeature.SetField("SourceX" , sourceX);
                tmpFeature.SetField("SourceY" , sourceY);
                tmpFeature.SetField("DestX" , pointSrc.getX());
                tmpFeature.SetField("DestY" , pointSrc.getY());
                tmpFeature.SetField("SrcClass" , srcClass);
                tmpFeature.SetField("DstClass" , dstClass);
                edgeCtr++;
                
                outLayer->CreateFeature(&tmpFeature);
            }
        }
        if(debug) std::cout << std::endl;
    }
    std::cout << "Total edges stored: " << edgeCtr << std::endl;
    
    outDataSource->SyncToDisk ();
    
}
