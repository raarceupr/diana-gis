//polysplit.h

#ifndef POLYSPLIT_H
#define POLYSPLIT_H

#define MAXVERTICES 100
#define OUTPUTDRIVER "ESRI Shapefile"
#define OUTPUTTYPE wkbPolygon
#define IDFIELD "id"

#include <cstdlib>
#include <iostream>
#include <vector>
#include <ogrsf_frmts.h>

typedef std::vector<OGRPolygon *> OGRPolyList;
typedef int feature_id_t;

static bool debug = false;

void split_polygons(OGRPolyList *pieces, OGRGeometry* geometry, int max_vertices);

OGRDataSource *create_destination(const char* drivername, const char* filename,
        const char *layername, const char *id_field_name);

void write_feature(OGRLayer *layer, OGRPolygon *geom, feature_id_t id);

#endif