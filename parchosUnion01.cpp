#include <gdal_priv.h>
#include <cpl_conv.h> // for CPLMalloc()
#include <string>
#include <ogrsf_frmts.h>
#include <gdal_priv.h>
#include <iostream>
#include <gdal.h>
#include <vector>
#include <map>
#include <cmath>
#include <iomanip>
#include <set>
#include <ogr_geometry.h> 


#include <iostream>
#include <vector>
#include <boost/assign/std/vector.hpp>
#include <boost/geometry.hpp>
#include <boost/geometry/algorithms/area.hpp>
#include <boost/geometry/algorithms/assign.hpp>
#include <boost/geometry/geometries/point_xy.hpp>
#include <boost/geometry/geometries/polygon.hpp>
#include <boost/geometry/io/dsv/write.hpp>
#include <deque>

#include "polysplit.h"

class intersectInfo {
public:
    double area;
    int count;
    intersectInfo(double a, int c) {
        area = a; count = c;
    }
};


using namespace boost::assign;
typedef boost::geometry::model::d2::point_xy<double> point_xy;
typedef boost::geometry::model::polygon<boost::geometry::model::d2::point_xy<double> > BoostPoly;


//
// Given a ogr style geometry returns a booost polygon.
//

void ogrPolyToBoost(OGRGeometry *oGeom, BoostPoly &bPoly){
    OGRPoint *tmpPt = new OGRPoint;
    std::vector<point_xy> points;
    OGRLinearRing *extRing = static_cast<OGRPolygon*>(oGeom)->getExteriorRing();
    
    std::cout << "Points in external ring: " << extRing->getNumPoints() << std::endl;
    for (int k = 0; k < extRing->getNumPoints(); k++) {
        extRing->getPoint (k, tmpPt);
        points.push_back(point_xy(tmpPt->getX(),tmpPt->getY()));
        // std::cout << (double)tmpPt->getX() << "," << tmpPt->getY() << " " ;
    }
    boost::geometry::assign_points(bPoly, points);
    // std::cout << std::endl;
    
    
    
    
    
    int innerRingCtr = 0;
    
    int innerPointCtr = 0;
    
    OGRLinearRing *intRing = static_cast<OGRPolygon*>(oGeom)->getInteriorRing(innerRingCtr);
    while (intRing!=NULL) {
        points.resize(0);
        
        bPoly.inners().resize(innerRingCtr+1);
        boost::geometry::model::ring<point_xy> &inner = bPoly.inners().back();
        
        for (int k = 0; k < intRing->getNumPoints(); k++) {
            intRing->getPoint (k, tmpPt);
            points.push_back(point_xy(tmpPt->getX(),tmpPt->getY()));
            // std::cout << (double)tmpPt->getX() << "," << tmpPt->getY() << " " ;
            innerPointCtr++;
        }
        
        boost::geometry::assign_points(inner, points);
        innerRingCtr++;
        intRing = static_cast<OGRPolygon*>(oGeom)->getInteriorRing(innerRingCtr);
    }
    std::cout << "Inner rings: " << innerRingCtr << " with a total of points: "  << innerPointCtr << std::endl;
    
    
    delete tmpPt;
    delete extRing;
}


//
// Given a layer and a vector of polygons, populates the vector with the polygons of that layer,
// the polygons are in boost style.
//

void populateBoostPolyVec(OGRLayer *layer, std::vector< BoostPoly > &boostPolyVec, std::vector <double> &areaVec) {
    OGRGeometry *oGeom;
    BoostPoly bPoly;
    for (int j = 0; j <  layer->GetFeatureCount(); j++) {
        std::cout << "Polygon " << j << std::endl;
        oGeom = layer->GetFeature(j)->GetGeometryRef();
        ogrPolyToBoost(oGeom, bPoly);
        boostPolyVec.push_back(bPoly);
        std::cout << "Polygon has an area of " << boost::geometry::area(bPoly) << std::endl;
        areaVec.push_back(boost::geometry::area(bPoly));
    }
}

//
// Given a deque that contains polygons acccumulates the areas of the polygons
// and returns total.
//
double boostPolyArea( std::deque<BoostPoly> &poly) { 
    double area = 0;
    // for(auto const p: poly)
    //     area += boost::geometry::area(p);
    
    while(!poly.empty()) {
        area += boost::geometry::area(poly.back());
        poly.pop_back();
    }
    return area;
}



void splitLayerGeometries(OGRLayer *srcLayer, OGRPolyList &pieces, std::vector<int> &pieceToID){
    OGRFeature *feature;
    // const char *id_field_name = NULL;
    
    
    
    int features_read = 0, features_written = 0,
            total = srcLayer->GetFeatureCount();
    
    srcLayer->ResetReading();
    int id_field = -1;
    while( (feature = srcLayer->GetNextFeature()) != NULL ) {
        /* Make an empty parts list, and get the ID and geometry from the
         * input. */
        OGRPolyList tmpPieces;
        //pieces.resize(0);
        int max_vertices = MAXVERTICES;
        
        feature_id_t id = (id_field >= 0 ? feature->GetFieldAsInteger("ID") //id_field)
                                         : feature->GetFID());
        OGRGeometry *geometry = feature->GetGeometryRef();
        
        /* Recursively split the geometry, and write a new feature for each
         * polygon that comes out. */
        split_polygons(&tmpPieces, geometry, max_vertices);
        for (OGRPolyList::iterator it = tmpPieces.begin(); it != tmpPieces.end(); it++) {
            
            if(debug) std::cout << "A polygon for id: " << id << std::endl;
            if(debug) std::cout << "Area : "  << static_cast<OGRSurface*>(*it)->get_Area() << std::endl;
            pieceToID.push_back(id);
            pieces.push_back(*it);
            //write_feature(destLayer, *it, id);
            //features_written++;
            
            
            /* We don't have to destroy each piece because write_feature calls
             * SetGeometryDirectly. */
        }
        
        features_read++;
        if (debug) {
            std::cerr << features_read << " / " << total << "\r";
            
            
        }
        
        OGRFeature::DestroyFeature( feature );
    }
    std::cout << "Total of polygons after split:" << features_written << "\n";
    
}

void usage(void) {
    std::cerr << "\nUsage: polysplit [opts] <input> <output>\n\n"
              << "\t-i\tinput layer name\n"
              << "\t-o\toutput layer name\n"
              << "\t-f\tOGR output driver name\n"
              << "\t-n\tID field name (must be integer type)\n"
              << "\t-m\tMax vertices per output polygon\n"
              << "\t-v\tVerbose mode\n\n";
    exit(1);
}

int main(int argc, char** argv) {
    
    
    
    // const char *source_name, *src_layer_name = NULL,
    //            *dest_name, *dest_layer_name = NULL,
    //            *driver_name = OUTPUTDRIVER,
    //            *id_field_name = NULL;
    int opt;
    std::string patchFN, habitatFN, outputFN;
    
    while ((opt = getopt(argc, argv, "p:h:o:n:m:v")) != -1) {
        switch (opt) {
        case 'p': patchFN   = optarg;     break;
        case 'h': habitatFN = optarg;     break;
        case 'o': outputFN  = optarg;     break;
            // case 'f': driver_name = optarg;         break;
            // case 'n': id_field_name = optarg;       break;
            // case 'm': max_vertices = atoi(optarg);  break;
        case 'v': debug = true;                 break;
        default: usage();
        }
    }
    argc -= optind;
    argv += optind;
    
    if (patchFN.length() == 0 || habitatFN.length() == 0 ) {
        std::cout << "Please provide the name of the habitat and patch shapefiles!!!" << std::endl;
        return 1;
    }
    
    
    // i
    
    
    // std::string pszFilename = "/work/rarce/research/diana/data/Vine_network/raw_input_layers/VinePatchesAll.shp";
    // std::string habitatFN = "/work/rarce/research/diana/data/Results_step0.25/lc_index_2c_poly.shp"; 
    
    // const char* path = "test.shp";
    OGRRegisterAll();
    OGRLayer *myLayer, *habitatLayer;
    OGRFeature *feature, *featureJ;
    OGRDataSource *hDS, *habitatDataSource;
    OGRSFDriver *driver;
    OGRSFDriverRegistrar *registrar =  OGRSFDriverRegistrar::GetRegistrar();
    OGRGeometry  *intersectGeom;
    OGREnvelope *habitatBBox = new OGREnvelope;
    OGREnvelope *patchBBox;
    
    std::vector <OGRFeature *> VF;
    std::vector <OGREnvelope *> VBBox;
    std::vector <OGRGeometry *> VGeom;
    std::vector <intersectInfo> VIntersect;
    
    std::vector <double> patchAreaVec, habitatAreaVec;
    
    std::deque<BoostPoly> output;
    
    
    // open the files
    driver = registrar->GetDriverByName("ESRI Shapefile");
    hDS = driver->Open(patchFN.c_str(),0);
    habitatDataSource = driver->Open(habitatFN.c_str(),true);
    
    
    //myLayer = 
    std::cout << hDS->GetLayerCount() << std::endl;;
    myLayer = hDS->GetLayer(0);
    habitatLayer = habitatDataSource->GetLayer(0);
    
    // These are the new split polygons
    OGRPolyList pieces;
    // The index is the split polygon index, the content is the original ID
    std::vector <int> pieceToID;
    splitLayerGeometries(habitatLayer, pieces, pieceToID);
    std::cout << "After split, number of polygons: " << pieces.size()<< std::endl;
    
    
    //std::vector <BoostPoly> boostPolyVecPatch, boostPolyVecHabitat;
    //populateBoostPolyVec(myLayer, boostPolyVecPatch, patchAreaVec);
    //populateBoostPolyVec(habitatLayer, boostPolyVecHabitat, habitatAreaVec);
    
    
    // This creates a new field
    // OGRFieldDefn testFieldDefn("test", OFTInteger);
    // habitatLayer->CreateField(&testFieldDefn);
    
    
    //hDS->CreateLayer("layer",NULL,wkbPoint,NULL);
    
    for (int j = 0; j <  myLayer->GetFeatureCount(); j++) {
        VF.push_back(myLayer->GetFeature(j));
        OGREnvelope *tmp = new OGREnvelope;
        VF[j]->GetGeometryRef()->getEnvelope(tmp);
        VBBox.push_back(tmp);
        VGeom.push_back(VF[j]->GetGeometryRef());
    }
    
    
    std::vector <double> VIntersectArea;
    VIntersectArea.resize(habitatLayer->GetFeatureCount()+1);
    for (auto &iArea: VIntersectArea) iArea = 0;
    
    
    std::vector < std::set< int > > VIntersectedPatches; 
    VIntersectedPatches.resize(habitatLayer->GetFeatureCount()+1);
    
    int pieceCtr = 0;
    int featureID;
    int prevID = -1;
    for (OGRPolyList::iterator it = pieces.begin(); it != pieces.end(); it++) {
        //for (int i = 0; i <  habitatLayer->GetFeatureCount(); i++) {
        //for (int i = 8758; i <= 8758; i++) {
        
        
        //feature = habitatLayer->GetFeature(i);
        featureID = pieceToID[pieceCtr++]; //feature->GetFieldAsInteger("ID");
        //featureGeom = feature->GetGeometryRef();
        //featureGeom->getEnvelope(habitatBBox);
        static_cast<OGRSurface*>(*it)->getEnvelope(habitatBBox); 
        //std::cout << featureID << " " <<  habitatBBox->MinX << std::endl;
        int ctr = 0;
        double intersectArea;
        
        for (int j = 0; j <  myLayer->GetFeatureCount(); j++) {
            //std::cout << "Patch: " << j << std::endl;
            featureJ = VF[j]; // = myLayer->GetFeature(j);
            patchBBox = VBBox[j];
            intersectArea = 0.0;
            if (habitatBBox->MinX < patchBBox->MaxX && 
                    habitatBBox->MaxX > patchBBox->MinX &&
                    habitatBBox->MinY < patchBBox->MaxY && 
                    habitatBBox->MaxY > patchBBox->MinY) {
                //std::cout  << "possible intersection at: " << featureID << " " << featureJ->GetFieldAsInteger("PatchID") << std::endl;
                
                //patchGeom = featureJ->GetGeometryRef();
                
                
                //boost::geometry::intersection(boostPolyVecPatch[j], boostPolyVecHabitat[i], output);
                
                
                // std::cout  << "According to boost:" << output.size() << std::endl;
                //if (output.size()) {
                
                if ( VGeom[j]->Intersects(static_cast<OGRGeometry*>(*it)) ) {
                    // std::cout  << featureID << " Intersects: " <<  featureJ->GetFieldAsInteger("PatchID") << std::endl;
                    
                    // std::cout << "Area according to boost: " << boostPolyArea(output) << std::endl;
                    
                    //intersectArea = boostPolyArea(output);
                    
                    
                    intersectGeom = VGeom[j]->Intersection(static_cast<OGRPolygon*>(*it));
                    
                    VIntersectedPatches[featureID].insert(j);
                    
                    if (intersectGeom->getGeometryType() == wkbPolygon)
                        intersectArea = static_cast<OGRSurface*>(intersectGeom)->get_Area();
                    else if  (intersectGeom->getGeometryType() == wkbMultiPolygon)
                        intersectArea = static_cast<OGRGeometryCollection*>(intersectGeom)->get_Area();
                    
                    
                    
                    //std::cout << "its a poly!!!" << std::endl;
                    // std::cout << "intersect type:" << intersectGeom->getGeometryType() << std::endl;
                    //std::cout << "area of intersect: " << static_cast<OGRSurface*>(intersectGeom)->get_Area() << std::endl; 
                    
                    
                    // if (intersectGeom->getGeometryType() == wkbMultiPolygon)
                    // std::cout << "area: " << static_cast<OGRGeometryCollection*>(intersectGeom)->get_Area() << std::endl; 
                    
                    
                    //intersectArea;// static_cast<OGRSurface*>(intersectGeom)->get_Area();
                    //if (intersectArea > 0.0) std::cout << "Variable area of intersect: " << intersectArea << std::endl; 
                    
                    ctr++;
                    //delete intersectGeom;
                    
                    //}
                }
                /*else if (patchAreaVec[j] ==0) {//( boost::geometry::area(boostPolyVecPatch[j]) == 0) {
                  
                std::cout  << featureID << " Patch: " <<  j << " has 0 boost area" << std::endl;
                
                    if ( VGeom[j]->Intersects(featureGeom) )  {
                        intersectGeom = VGeom[j]->Intersection(featureGeom);
                        std::cout << "type: " << intersectGeom->getGeometryType() << std::endl;
                        intersectArea = 0.0;
                        if (intersectGeom->getGeometryType() == wkbPolygon)
                            intersectArea = static_cast<OGRSurface*>(intersectGeom)->get_Area();
                            
                            
                        else if  (intersectGeom->getGeometryType() == wkbMultiPolygon) {
                            for (int r = 0; r < static_cast<OGRGeometryCollection*>(intersectGeom)->getNumGeometries(); r++) 
                                intersectArea += static_cast<OGRSurface*>(static_cast<OGRGeometryCollection*>(intersectGeom)->getGeometryRef(r))->get_Area();
                                
                        }
                        //     intersectArea = static_cast<OGRGeometryCollection*>(intersectGeom)->get_Area();
                        area += intersectArea;
                        ctr++;
                    }
               } 
               */
                
            }
            
            //if (area > 0.0) std::cout << "Updateing area of intersect for featureID: " << featureID << " to: " << area << std::endl; 
            VIntersectArea[featureID] += intersectArea;
            //if (featureID==1437)std::cout << "Now: " << VIntersectArea[featureID] << std::endl; 
            
            // if (j%100==0) std::cout << "i, j:" << i << ","  << j <<std::endl;
            
            //featureJ->GetGeometryRef()->getEnvelope(bbox2);
            //std::cout  << VBBox[j]->MinX   << " " ;
            
        }
        
        /*
        int habitatPatches = feature->GetFieldAsInteger("patches");
        bool different = std::abs(habitatArea - area) > 0.1 || ctr != habitatPatches;
        
        
        std::cout << std::fixed << std::setprecision(4);
        std::cout  << (different ? "Different!!!!" :"") 
                   << "Patch area and count for " <<  featureID << ": " << area << " " << ctr 
                   << " vs "  << habitatArea << ", "  << habitatPatches << std::endl;
        VIntersect.push_back(intersectInfo(area,ctr));
        */
        if (prevID > 0 && prevID != featureID) {
            
            feature = habitatLayer->GetFeature(prevID);
            
            double habitatArea = feature->GetFieldAsDouble("PatchArea");
            int habitatPatches = feature->GetFieldAsInteger("patches");
            
            int patchesInserted = VIntersectedPatches[prevID].size();
            
            if (std::abs ( VIntersectArea[prevID] - habitatArea) / habitatArea  > 0.001 || 
                    patchesInserted != habitatPatches ) std::cout << "Diff: "; 
            std::cout << "Done with " << prevID <<  " area: " << VIntersectArea[prevID] 
                         << " vs "  << habitatArea 
                         << "ctr: " << patchesInserted << " vs " << habitatPatches << std::endl;
            
            //   std::cout << "patches inserted: " << VIntersectedPatches[prevID].size() << std::endl;             
            // for (std::set<int>::iterator sit = VIntersectedPatches[prevID].begin(); 
            //   sit != VIntersectedPatches[prevID].end(); sit++) std::cout << "(" << *sit << ") ,";
            // std::cout << std::endl;
        }
        //if (featureID == 1437) exit(1);
        prevID = featureID;
    }
}
